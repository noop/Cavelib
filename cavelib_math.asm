// This is cribbed from the KickAssembler manual
// When used, it retrieves the next byte in a 16bit argument 
//
.function _16bitnextArgument(arg)
{
           .if (arg.getType()==AT_IMMEDIATE)
           .return CmdArgument(arg.getType(),>arg.getValue())
           .return CmdArgument(arg.getType(),arg.getValue()+1)
}


// Adds 16bit numbers [one] and [two], writes result to a 16bit target
//
// Clobbers: A
//
.pseudocommand add_words one : two : target
{
           clc                                   // clear carry
           lda one                               // Add low bytes
           adc two                               //
           sta target                            // Store sum in LSB

           lda _16bitnextArgument(one)           // Add high bytes using carry bit
           adc _16bitnextArgument(two)           //
           sta _16bitnextArgument(target)        // Store sum in MSB
}


// Adds a byte(8 bit) to a word(16 bit) and writes result to target (16 bit)
//
// Clobbers: A
//
.pseudocommand add_word_byte w : b : target
{
           clc                                   // clear carry
           lda w                                 // Add low bytes
           adc b                                 //
           sta target                            // Store sum in LSB

           lda _16bitnextArgument(w)             // Add high bytes using carry bit
           adc #$00                              // (implied 0 high byte for b)
           sta _16bitnextArgument(target)        // Store sum in MSB
}


// Subtracts 16bit numbers [one] and [two], writes result to a 16bit target
//
// Clobbers: A
//
.pseudocommand sub_words one : two : target
{
           sec                                   // set carry (sbc operation is inverse)
           lda one                               // subtract low bytes
           sbc two                               //
           sta target                            // store result in LSB

           lda _16bitnextArgument(one)           // subtract high bytes using carry bit
           sbc _16bitnextArgument(two)           //
           sta _16bitnextArgument(target)        // store result in MSB
}

// Subtracts a byte(8 bit) from a word(16 bit) and writes result to target (16 bit)
//
// Clobbers: A
//
.pseudocommand sub_word_byte w : b : target
{
           sec                                   // set carry (sbc operation is inverse)
           lda w                                 // subtract low bytes
           sbc b                                 //
           sta target                            // store result in LSB

           lda _16bitnextArgument(w)             // subtract high bytes using carry bit
           sbc #$00                              // (implied 0 high byte for b
           sta _16bitnextArgument(target)        // store result in MSB
}
