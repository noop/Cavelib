// -----------------------
// Memory Block Operations
// -----------------------
// NOTE: The subroutines use self-modifying code 
//       and due to that are not safe to call again
//       before having finished during run-time.

.var subroutine_memset  = $0000
.var subroutine_memxor  = $0000
.var subroutine_memcpy  = $0000
.var subroutine_gapcpy  = $0000
.var subroutine_memswap = $0000

.var sm_ms_val = $0000
.var sm_ms_p0  = $0000

.var sm_mx_val = $0000
.var sm_mx_ps  = $0000
.var sm_mx_p0  = $0000

.var sm_mc_p0  = $0000
.var sm_mc_p1  = $0000

// see also "memset" further down
.pseudocommand place_subroutine_memset
{
  .if (subroutine_memset == $0000) 
  {  
    .eval subroutine_memset = *  
    .print getFilename() + ": subroutine_memset = " + address_of(subroutine_memset)
  
    loop:
    // condition code in status comes from before loop or before jmp near tail
    bne set // if high byte of count != 0
    txa
    beq done // if count == 0 
    
    set:
    .eval sm_ms_val = *+1 
    lda #$DA // load from self modifying value
    .eval sm_ms_p0 = *+1 
    sta $BEEF // store to self modifying pointer
      
    inc sm_ms_p0 // inc low byte of target pointer
    bne skipped_target_high_inc
    inc sm_ms_p0+1 // inc high byte of target pointer
    skipped_target_high_inc:
      
    // adjust count
    txa
    beq dec_high_and_low_maybe
    dex
    tya
    jmp loop
    dec_high_and_low_maybe:
    tya 
    beq done // if count == 0
    dex
    dey
    jmp loop
    done:
    rts
  }
}

// see also "memxor" further down
.pseudocommand place_subroutine_memxor
{
  .if (subroutine_memxor == $0000) 
  {  
    .eval subroutine_memxor = *  
    .print getFilename() + ": subroutine_memxor = " + address_of(subroutine_memxor)

    loop:
    // condition code in status comes from before loop or before jmp near tail
    bne set // if high byte of count != 0
    txa
    beq done // if count == 0 
    
    set:
    .eval sm_mx_ps = *+1 
    lda $DEAD // load from self modifying value
    .eval sm_mx_val = *+1 
    eor #$DE 
    .eval sm_mx_p0 = *+1
    sta $BEEF // store to self modifying pointer
    
    inc sm_mx_p0 // inc low byte of target pointer
    inc sm_mx_ps
    bne skipped_target_high_inc
    inc sm_mx_p0+1 // inc high byte of target pointer
    inc sm_mx_ps+1
    skipped_target_high_inc:
    
    // adjust count
    txa
    beq dec_high_and_low_maybe
    dex
    tya
    jmp loop
    dec_high_and_low_maybe:
    tya 
    beq done // if count == 0
    dex
    dey
    jmp loop
    done:  
    rts
  }
}

// see also "memcpy" further down
// see also "gapcpy" further down
// (NOTE: if you change this, also update :gapcpy which relies on this one)
.pseudocommand place_subroutine_memcpy
{
  .if (subroutine_memcpy == $0000) 
  {  
    .eval subroutine_memcpy = *  
    .print getFilename() + ": subroutine_memcpy = " + address_of(subroutine_memcpy)

    loop:
    // condition code in status comes from before loop or before jmp near tail
    bne set // if high byte of count != 0   
    txa
    beq done // if count == 0
    
    set:
    .eval sm_mc_p1 = *+1 
    lda $DEAD // load from self modifying pointer
    .eval sm_mc_p0 = *+1 
    sta $BEEF // store to self modifying pointer
    
    // adjust self modifying code pointers
    inc sm_mc_p1 // inc low byte of source pointer
    bne skipped_source_high_inc
    inc sm_mc_p1+1 // inc high byte of source pointer if low byte just wrapped around
    skipped_source_high_inc:
    inc sm_mc_p0 // inc low byte of target pointer
    bne skipped_target_high_inc
    inc sm_mc_p0+1 // inc high byte of target pointer if low byte just wrapped around
    skipped_target_high_inc:
    
    
    // adjust count
    txa
    beq dec_high_and_low_maybe
    dex
    tya
    jmp loop
    dec_high_and_low_maybe:
    tya 
    beq done // if count == 0
    dex
    dey
    jmp loop
    done:  
    rts
  }
}

// see also "memcpy" above
// see also "gapcpy" further down
.pseudocommand place_subroutine_gapcpy
{
  .if (subroutine_gapcpy == $0000) 
  {  
    .eval subroutine_gapcpy = *  
    .print getFilename() + ": subroutine_gapcpy = " + address_of(subroutine_gapcpy)
    
    .var s = cl_w0
    .var t = cl_w1
    .var sl = cl_w2
    .var tl = cl_w3
    .var c  = cl_b0
    .var bc = cl_b1 // and cl_b2
        
    lda c
    beq done
        
    go_again: 
      :fetch_pointer sm_mc_p1 : s // source and
      :fetch_pointer sm_mc_p0 : t // target updated in each iteration
        
      ldx bc
      ldy bc+1
        
      jsr subroutine_memcpy
        
      dec c
      beq done
        
      :add_words s : sl : s // skip line length in source
      :add_words t : tl : t // skip line length in target
        
      jmp go_again
    done:
    
    rts
  }
}

// see also "memswap" further down
.pseudocommand place_subroutine_memswap
{
  .if (subroutine_memswap == $0000) 
  {  
    .eval subroutine_memswap = *  
    .print getFilename() + ": subroutine_memswap = " + address_of(subroutine_memswap)

    loop:
    // condition code in status comes from before loop or before jmp near tail
    bne set // if high byte of count != 0   
    txa
    beq done // if count == 0
    
    set:
    sty cl_b0 // preserve y
    
    // swap bytes
    ldy #$00
    lda (cl_w0),y // 5
    sta cl_b1     // 3
    lda (cl_w1),y // 5
    sta (cl_w0),y // 6
    lda cl_b1     // 3
    sta (cl_w1),y // 6  
    
    ldy cl_b0 // restore y
    
    // adjust pointers
    inc cl_w0 // inc low byte of source pointer
    bne skipped_source_high_inc
    inc cl_w0+1 // inc high byte of source pointer if low byte just wrapped around
    skipped_source_high_inc:
    inc cl_w1 // inc low byte of target pointer
    bne skipped_target_high_inc
    inc cl_w1+1 // inc high byte of target pointer if low byte just wrapped around
    skipped_target_high_inc:
    
    
    // adjust count
    txa
    beq dec_high_and_low_maybe
    dex
    tya
    jmp loop
    dec_high_and_low_maybe:
    tya 
    beq done // if count == 0
    dex
    dey
    jmp loop
    done:  
    rts
  }
}


// target = any address expression
// count = number of bytes to set as a 16bit value
// value = the 8bit value to write to each address starting at target   
.pseudocommand memset target : count : value 
{ 
  // make sure the subroutine was defined first 
  .if ( subroutine_memset == $0000 )
    .error ":memset used without :place_subroutine_memset"

  :fetch_pointer sm_ms_p0 : target
  
  lda value
  sta sm_ms_val
  
  ldx get_low(count)
  ldy get_high(count)
  
  jsr subroutine_memset
} 

// target = any address expression
// count = number of bytes to modify as a 16bit value
// value = the 8bit value to XOR with each address starting at target   
.pseudocommand memxor target : count : value 
{ 
  // make sure the subroutine was defined first 
  .if ( subroutine_memxor == $0000 )
    .error ":memxor used without :place_subroutine_memxor"

  :fetch_pointer sm_mx_p0 : target
  :fetch_pointer sm_mx_ps : target
  
  lda value
  sta sm_mx_val
  
  ldx get_low(count)
  ldy get_high(count)
  
  jsr subroutine_memxor
} 

// target = any address expression
// source = any address expression
// count = number of bytes to copy as a 16bit value   
.pseudocommand memcpy target : source: count 
{ 
  // make sure the subroutine was defined first 
  .if ( subroutine_memcpy == $0000 )
    .error ":memcpy used without :place_subroutine_memcpy"

  :fetch_pointer sm_mc_p0 : target
  :fetch_pointer sm_mc_p1 : source
  
  ldx get_low(count)
  ldy get_high(count)
  
  jsr subroutine_memcpy
}

// this one uses subroutine_memcpy in a loop 
//
// target = any address expression
// source = any address expression
//    
// sl_length  (16 bit) = the distance between two whole lines in bytes in source (the gap)
//  b_copy    (16 bit) = the number of bytes per line to copy
// tl_length  (16 bit) = the distance between two whole lines in bytes in target (the gap) 
// count       (8 bit) = the number of lines(or parts of lines if b_copy<sl_length) to copy
.pseudocommand gapcpy target : source:  sl_length : b_copy : tl_length : count  
{ 
  // make sure the subroutine was defined first 
  .if ( subroutine_gapcpy == $0000 )
    .error ":gapcpy used without :subroutine_gapcpy"

  .var s = cl_w0
  .var t = cl_w1
  .var sl = cl_w2
  .var tl = cl_w3
  .var c  = cl_b0
  .var bc = cl_b1 // and cl_b2

  :fetch_pointer t : target
  :fetch_pointer s : source 
  :fetch_pointer sl : sl_length
  :fetch_pointer tl : tl_length
  :fetch_pointer bc : b_copy
  :set_bits c : count
  
  jsr subroutine_gapcpy
}

// target = any address expression
// source = any address expression
// count = number of bytes to swap as a 16bit value   
//
// clobbers cavelib zeropage vars: cl_w0, cl_w1, cl_b0, cl_b1
.pseudocommand memswap target : source : count 
{ 
  // make sure the subroutine was defined first 
  .if ( subroutine_memswap == $0000 )
    .error ":memswap used without :place_subroutine_memswap"

  :fetch_pointer cl_w0 : source
  :fetch_pointer cl_w1 : target
  
  ldx get_low(count)
  ldy get_high(count)
  
  jsr subroutine_memswap
}

// ------------------------------------------------------------------------
// special zero page versions for set and copy

// target = any address(in zero page)
// count = number of bytes to set as a 16bit value
// value = the 8bit value to write to each address starting at target   
// no sanity checks are made whether target+count wraps around in zp, so be careful
.pseudocommand zp_memset target : count : value { 
  lda target
  sta sm_p0+1
  lda value
  sta sm_p1+1
  
  ldx count
  
  loop:
  // condition code in status comes from before loop or before jmp near tail
  beq done // if count = 0 end loop
  
  set:
  sm_p1: lda #$DE // load from self modifying value
  sm_p0: sta $BE // store to self modifying pointer
  
  // adjust self modifying code pointer
  inc sm_p0+1 // inc target pointer
  
  // adjust count
  dex
  bne set // if count != 0 loop
  done:
} 

// target = any address(in zero page)
// source = any address(in zero page)
// count = number of bytes to copy as a 8bit value  
// no sanity checks are made whether target+count wraps around in zp, so be careful  
.pseudocommand zp_memcpy target : source: count { 
  lda target
  sta sm_p0+1
  lda source
  sta sm_p1+1
  
  ldx count
  
  loop:
  // condition code in status comes from before loop or before jmp near tail
  beq done // if count = 0 end loop
  
  set:
  sm_p1: lda $DE // load from self modifying pointer
  sm_p0: sta $BE // store to self modifying pointer
  
  // adjust self modifying code pointers
  inc sm_p1+1 // inc source pointer
  inc sm_p0+1 // inc target pointer
  
  // adjust count
  dex
  bne set // if count != 0 loop
  done:
}
