// Cavelib
// example 01 - hello world


#import "cavelib.asm"

.const tf = 6*50 // total frames


:begin_zp_user_vars
  frames: .word tf // frame counter (caveat: initialization like this is not supported for zero page vars!)
:end_zp_user_vars 
   


.pc = $0801 "Basic Upstart"
:BasicUpstart(main)
  


:begin_slow_ram_vars_and_routines
  hello: :senc_string(0, "hello world!") // encode string in char codes, first two bytes will be the length in bytes
:end_slow_ram_vars_and_routines
   


main:
  :init_cavelib  
  // asserting a specific version at assembly time is optional
  // but recommended as future changes to cavelib might
  // break backward compatibility
  assert_cavelib_version("n0.2d_dev0")

  // init zero-page var for real now
  lda #<tf
  sta frames
  lda #>tf
  sta frames+1

  // clear text screen in bank 0, screen number 1, with space char, write white to color ram
  :clear_text_screen #0 : #1 : #' ' : #WHITE
  // place the hello string at center of line 12 in bank 0, screen number 1
  :memcpy #VIC2_SCREEN(0,1)+12*40+14 : #hello+2 : hello
  
  :init_irq_by_raster_only #at_vblank : #PAL_VBLANK_START 

  main_loop:
    nop
    
    // countdown reached 0 ?
    ldy frames+1
    cpy #0
    bne main_loop
    ldx frames
    cpx #0
    bne main_loop
    
    :disable_raster_compare_irq
    
  end_main:
  :shutdown_cavelib #0
  
  
  
at_vblank:
    :begin_irq_handler
    
    ldx frames
    stx VIC2_BORDER_COLOR
    
    // frame countdown logic
    :sub_word_byte frames : #1 : frames
    
    low_only:
    
    :ack_vic2_irq_all
    :end_irq_handler