// --------------------------------------------------------------
// File Loading 
// (need KERNAL switched on (use :toggle_KERNAL before/after use)
//  and thus will clobber some zero page vars between
//  $90 and $ff )
// --------------------------------------------------------------

// ---------------------
// compile time switches
// ---------------------
// this device number will be used instead when a request to use the last used
// device would result in device number 0
.var default_device_number = $08

// stores the registers A X Y and status P
// at the otherwise unused RAM at $03fc to $03ff
.var subroutine_load_error_store = $0000
.pseudocommand place_subroutine_load_error_store 
{ 
  .if ( subroutine_load_error_store == $0000 )
  {
    .eval subroutine_load_error_store = *
    .print getFilename() + ": subroutine_load_error_store = " + address_of(subroutine_load_error_store)
    
    .print getFilename() + " says:"
    .print "      after-file-error states of A X Y P are at $03fc $03fd $03fe $03ff"
    .print "      (decimal 1020, 1021, 1022, 1023 (for PEEK))"
    .print "      any file error will break to BASIC after cleanup "
    
    php
    sta $03fc; stx $03fd; sty $03fe 
    pla
    sta $03ff
    rts  
    
// register A most likely contained the error code when error occured
//
// kernal error codes from: http://sta.c64.org/cbm64krnerr.html
//
//I/O ERROR #1  TOO MANY FILES        $01 1 $F6FB
//I/O ERROR #2  FILE OPEN             $02 2 $F6FE
//I/O ERROR #3  FILE NOT OPEN         $03 3 $F701
//I/O ERROR #4  FILE NOT FOUND        $04 4 $F704
//I/O ERROR #5  DEVICE NOT PRESENT    $05 5 $F707
//I/O ERROR #6  NOT INPUT FILE        $06 6 $F70A
//I/O ERROR #7  NOT OUTPUT FILE       $07 7 $F70D
//I/O ERROR #8  MISSING FILENAME      $08 8 $F710
//I/O ERROR #9  ILLEGAL DEVICE NUMBER $09 9 $F713 
  }
}
.pseudocommand load_error_store
{
  .if ( subroutine_load_error_store == $0000 )
    .error ":load_error_store used without :place_subroutine_load_error_store"
  
  jsr subroutine_load_error_store
}

// this command is used after properly cleaning up any errors 
// that may have occured in any of the file handling routines in here
.pseudocommand load_error_handler 
{ 
  // TODO(if there is time): implement some more convenient way
  // of reporting the error before breaking out of execution
  
  :shutdown_cavelib #0 // for now just break all further execution
}
                                 
// ----------------------
// kernal routine symbols
// ----------------------
.var sr_kernal_setnam = $ffbd
.var sr_kernal_setlfs = $ffba
.var sr_kernal_load   = $ffd5
.var sr_kernal_open   = $ffc0
.var sr_kernal_chkin  = $ffc6
.var sr_kernal_readst = $ffb7
.var sr_kernal_chrin  = $ffcf
.var sr_kernal_close  = $ffc3
.var sr_kernal_clrchn = $ffcc

// -------------------------------------------------
// synchronous atomic high level kernal LOAD wrapper
// -------------------------------------------------

// addresses for self modifying values inside the load_sync_subroutine
.var sm_ls_sr_fnl = $0000
.var sm_ls_sr_fn_low = $0000
.var sm_ls_sr_fn_high = $0000
.var sm_ls_sr_dn = $0000
.var sm_ls_sr_sa = $0000
.var sm_ls_sr_ltal = $0000
.var sm_ls_sr_ltah = $0000
.var subroutine_load_sync = $0000


// generates the code for this subroutine at the current position
// the routine itself can then be prepared and called using the pseudocommand below
// which will set all the self-modifying parameters before calling the routine
.pseudocommand place_subroutine_load_sync
{
  .if( subroutine_load_sync == $0000) // make sure this code is only generated once
  { 
    .eval subroutine_load_sync = *
    .print getFilename() + ": subroutine_load_sync = " + address_of(subroutine_load_sync)
    
    .eval sm_ls_sr_fnl = *+1 
    lda #$FF // file name length (self modifying value)
    nop // fill nop needed to prevent string length determiner from overwriting the next instruction
    .eval sm_ls_sr_fn_low = *+1 
    ldx #$FF // file name start low byte (self modifying value) 
    .eval sm_ls_sr_fn_high = *+1
    ldy #$FF // file name start high byte (self modifying value)
  
    jsr sr_kernal_setnam
  
    lda #$01 // logical file number 1
    .eval sm_ls_sr_dn = *+1
    ldx #$FF // device number (self modifying value)
    bpl skipped_use_last_device
    ldx $BA  // set device number to last used device number if given device number was negative
    bne skipped_use_last_device
    ldx #default_device_number  // use default device if last used device number is 0
    skipped_use_last_device:
  
    .eval sm_ls_sr_sa = *+1
    ldy #$FF // set secondary address (self modifying value)
    bne load_to_address_in_file // if y != 0
  
    load_to_specific_address:
    jsr sr_kernal_setlfs
    lda #$00 // mode = LOAD ( not 0 would be VERIFY )
    .eval sm_ls_sr_ltal = *+1
    ldx #$FF // set load target address low byte (self modifying value)
    .eval sm_ls_sr_ltah = *+1
    ldy #$FF // set load target address high byte (self modifying value)
    jsr sr_kernal_load
    bcs handle_error
    rts
  
    load_to_address_in_file:
    jsr sr_kernal_setlfs
    lda #$00 // mode = LOAD ( not 0 would be VERIFY ) 
    jsr sr_kernal_load
    bcs handle_error
    rts
  
    handle_error:
    :load_error_store
    :load_error_handler
    rts
  }
}


// synchronous loading of a file
// cstring_fname = the address where the file name starts in RAM
//                 must be terminated with a 0 byte
// device = the device number to load from (set to #$FF (-1) to use last used device number)
// address = the address to load to (set to $0000 to load to address stored inside file) 
// 
// NOTE: the kernal LOAD routine will ALWAYS skip the first two bytes in the file
//       (as it always assumes they contain a hexadecimal load address)
//       so the first byte ending up at "address" is always the third byte from file
//       no matter if it is supposed to load to a specified address
//                 or to the address in the first two bytes of the file
//
.pseudocommand load_sync cstring_fname : device : address
{ 
  // make sure the subroutine was defined first 
  .if ( subroutine_load_sync == $0000 )
    .error ":load_sync used without :place_subroutine_load_sync"
  
  // prepare self-modifying code variable values
  :cstring_length cstring_fname : sm_ls_sr_fnl // clobbers a NOP
  lda #NOP
  sta sm_ls_sr_fnl+1 // put NOP back in
  
  lda get_low(cstring_fname)
  sta sm_ls_sr_fn_low // set low byte of file name start address
  lda get_high(cstring_fname)
  sta sm_ls_sr_fn_high // set high byte of file name start address
  
  lda device
  sta sm_ls_sr_dn // set device number
  
  .if(address.getValue() == 0)
  {
    //.print "compile time branch, load_sync to address in file"
    ldy #$01
    sty sm_ls_sr_sa // set secondary address ( 1 = load to address in file )
  }
  else
  {
    //.print "compile time branch, load_sync to address " + address_of(address.getValue())
    ldy #$00
    sty sm_ls_sr_sa // set secondary address ( 0 = load to address in X and Y )
    ldx get_low(address)
    stx sm_ls_sr_ltal // set load address low
    ldy get_high(address)
    sty sm_ls_sr_ltah // set load address high
  }
  
  // call the now prepared subroutine
  :call_safe subroutine_load_sync 
}


////////////////////////////////////////////////////////////////////////////////


// ---------------------------------
// low level kernal routine wrappers
// ---------------------------------

// prepares the registers for SETNAM and calls it
// fname_start = the address where the filename string starts
// fname_end   = the address right behind the filename string
.pseudocommand kernal_setnam fname_start : fname_end 
{
  lda CmdArgument(AT_IMMEDIATE, fname_end.getValue()-fname_start.getValue()) 
  ldx get_low(fname_start)
  ldy get_high(fname_start)
  jsr sr_kernal_setnam
}

// prepares the registers for SETLFS and calls it
// file_num = logical file number to use
// device = device number ($BA = last used device number, #$xx device xx)
// secondary_address = e.g. 0, 1, 2,.. meaning depends on what calls are made after
//                     if the next call 
//                     is a "load" 0 means load to address in X/Y                      
//                     while !0 would mean load to address stored inside bytes 0..1                      
//                     of the file itself                      
.pseudocommand kernal_setlfs file_num : device : secondary_address
{
  lda file_num
  ldx device
  bne skipped_use_default_device_instead
  ldx #default_device_number // use default device number if last used was 0
  skipped_use_default_device_instead:
  ldy secondary_address
  jsr sr_kernal_setlfs
}

// calls OPEN
// if the carry flag is set, there was an error (error code in register A)
// otherwise all is well
.pseudocommand kernal_open
{
  jsr sr_kernal_open
}

// calls CHKIN
// file_num = the logical file number to use for input from following CHRIN
// use READST afterwards to check/handle errors
.pseudocommand kernal_chkin file_num
{
  ldx file_num
  jsr sr_kernal_chkin
}

// not an actual kernal call but puts the given address where
// a kernal LOAD would put it (used in byte-by-byte file reading here)
// in zero page ($AE and $AF)
.pseudocommand kernal_set_load_address address
{
  lda get_low(address)
  sta $AE
  lda get_high(address)
  sta $AF
}

// calls READST
// status will be in register A
// not 0 means end of file or read error
//       if bit 6 is set (test with "and #%01000000" followed by beq")
//       there was a read error (for details check drive error channel)
//       otherwise the end of file was reached
// 0 means status is ok and the next byte can be acquired with CHRIN
.pseudocommand kernal_readst
{
  // jsr sr_kernal_readst  
  lda $90 // Kernal I/O Status is always at zero page $90
}

// calls CHRIN
// to read the next byte from a file 
// (use READST before this and make sure there is no error and no end of file)
// ( the file must have been successfully opened for input by 
//  SETNAM, SETLFS, OPEN, CHKIN )
// register A will contain the byte read
.pseudocommand kernal_chrin
{
  jsr sr_kernal_chrin 
}

// not an actual kernal call but increases the address at $AE and $AF
// which the kernal LOAD would use as the target address pointer for read bytes
.pseudocommand kernal_inc_load_address
{
  inc $AE
  bne skipped_high
  inc $AF
  skipped_high:
}

// calls CLOSE
// file_num = the logical file number to close
.pseudocommand kernal_close file_num
{
  lda file_num
  jsr sr_kernal_close
}

// calls CLRCHN
.pseudocommand kernal_clrchn
{
  jsr sr_kernal_clrchn
}


////////////////////////////////////////////////////////////////////////////////


// -----------------------------------------------------------------------------
// high level wrappers for kernal byte-by-byte file reading which can be used
// inside interrupts to implement asynchronous file loading
// (above are low level wrappers for all the kernal ROUTINES in capital letters) 
//
// NOTE: these must be used in combination to operate on a single file at a time
//       DO NOT USE load_open AGAIN BEFORE THE FILE IS CLOSED
//
// -----------------------------------------------------------------------------

// wraps SETNAM, SETLFS, OPEN, CHKIN to open a file for reading
// (use READST before reading bytes with CHRIN after this to check for errors)
// 
// will brk on error
.pseudocommand load_open fname_start : fname_end : file_num : device : secondary_address
{
  :kernal_setnam fname_start : fname_end
  :kernal_setlfs file_num : device : secondary_address
  :kernal_open
  bcc open_ok
  
  :load_error_store
  :kernal_close file_num // file must be closed even if open failed
  :kernal_clrchn
  :load_error_handler
  
  open_ok:
  :kernal_chkin file_num
  
  // file is now open for input
}

// set the address to which fetch_byte (see below) 
// will put the next read byte
// ( use only once before consecutive calls to fetch_byte ) 
.pseudocommand set_fetched_bytes_start_address address
{
  :kernal_set_load_address address
}

// reads the next byte from the file opened with load_open
// branch_eof_or_error = where to branch on end of file or error
// 
// register A contains the read byte (unless the branch was taken)
// 
// NOTE: this can be used also to skip bytes as the read
//       byte is not stored anywhere except in register A
.pseudocommand read_byte branch_eof_or_error  
{ 
  :kernal_readst
  bne CmdArgument(AT_ABSOLUTE, branch_eof_or_error.getValue() + 3) // magical skip
  :kernal_chrin 
} 

// fetches the next byte from the file, stores it at the current
// target address and then increases the target address by 1
//
// branch_eof_or_error = where to branch on end of file or error 
// omit_y_set = set to 1 to skip setting Y register to 0 before the store
//
// will brk on error
//
// register A will still contain the read byte              
.pseudocommand fetch_byte branch_eof_or_error : omit_y_set
{
  :read_byte branch_eof_or_error
  .if (omit_y_set.getValue() != 1) ldy #$00
  sta ($AE),y
  :kernal_inc_load_address
}

// generates code to use for handling eof or error from read_byte or fetch_byte
// ( put a label in front like ".label eof_or_error_branch = *" to use  
//   as the branch parameter for those commands ) 
// 
// file_num   = file number to close on eof or error 
// 
// will brk on error 
.pseudocommand place_eof_or_error_handling file_num
{
  jmp all_good // magical skipper (so this is not executed even if there was no error) 
  // (machine code for the above instruction is 3 bytes long)
  
  // enter here on an actual error from routines above
  
  and #$40 // clear out all but the eof indicator bit (if it is set)  
  // if the eof was not set A is now 0, otherwise it is #$40
  pha
  bne eof
  
  // save error information
  :load_error_store  
  
  eof:
  :kernal_close file_num
  :kernal_clrchn
  pla
  bne all_good
  :load_error_handler
  
  all_good:
}
