// ----------------------------
// assembly time import helpers
// ----------------------------

//
//
// in_file     : must be the filename to import
// first_byte  : the first byte to import (count starts at 0, so to skip the first two bytes, set this to 2, which will start with the third byte)
// size        : the total number of bytes to import
//
// code_patches  : must be a list containing quintets: code_begin, code_end, abs_source_begin, abs_source_end, abs_target_base
//               where: code_begin : start of area in which to look for absolute addresses to replace (must be relative to 0 + first_byte) 
//                      code_end   : end of area in which to look for absolute addresses to replace (must be relative to 0 + first_byte)
//                      abs_source_begin : range of absolute addresses to look for.. 
//                      abs_source_end   : ..in the give code area..
//                      abs_target_base  : first address for new range of rewritten absolute addresses
//
// data_patches  : must be a list containing quartetts: data_begin, data_end, original_base_value, patch_base_value (or null)
// data_p_offs   : offset0, offset1, .. (offsets relative to 0 + firstbyte) in any of the ranges defined in data_patches (or null)
//
// debug_verbosity : 1 for minimal messages, 2 to message every patched location 
//
// This macro imports a files bytes and while doing so patches any addresses belonging to ABS-mode-opcodes in selectable code regions to a different base address.  
// (the address regions are not checked, if they overlap, only the first found address range is getting patched, also there is of course no sanity check to see
//  if the given code regions contain any valid code, it is assumed that they do)   
// 
// In addition to the address patches, manual data_patches ranges and data_p_offs can be supplied.
// The data_patches take precedence over code_patches and if a data patch was made, no further changes will be applied to the currently observed incoming byte.
// (also, as in the code_patches, only the first found patch is applied)
// 
//
.macro import_chopped_and_patch_absolute_opcode_addresses(in_file , first_byte , size , code_patches, data_patches, data_p_offs, debug_verbosity)
{  
  .var d = debug_verbosity
  .var import_address = *
  * = * "filled by 'import_chopped_and_patch_absolute_opcode_addresses'"
  .if (d > 0)
  {
          .print getFilename() + ": in 'import_chopped_and_patch_absolute_opcode_addresses' received parameters:"
          .print "        in_file: " + in_file
          .print "     first_byte: $" + toHexString(first_byte,4)
          .print "           size: $" + toHexString(size,4)
          .print "   code_patches: " 
          .if ( code_patches.size() / 5 > 0 && mod(code_patches.size(),5) == 0 ) 
          {
            .for(var i=0; i<code_patches.size(); i+=5)
            {
              .print "                 ( code_begin: $" + toHexString(code_patches.get(i+0),4) + " code_end : $" + toHexString(code_patches.get(i+1),4) + " abs_source_begin : $" + toHexString(code_patches.get(i+2),4) + " abs_source_end : $" + toHexString(code_patches.get(i+3),4) + " abs_target_base : $" + toHexString(code_patches.get(i+4),4) + " )"
            }
          }
          .if (data_patches != null)
          {
            .print "   data_patches: " + toIntString(data_p_offs.size()) + " offset/s to be patched" 
          }
          
          .print "  importing at address $" + toHexString(*,4)
  }
  
  
  .if (data_patches != null && data_patches.size() / 4 > 0 && mod(data_patches.size(),4) != 0)
    .error "data_patches must contain value quartetts"
   
    
  .if ( code_patches.size() / 5 > 0 && mod(code_patches.size(),5) == 0 ) // if list contains quintets  
  {
    .var fp = first_byte; // file pointer (index of current byte)
    .var opc = 0;
    .var ops = 0;
    .var patched_addresses = 0;
    .var file = LoadBinary(in_file)
    
    .var patched_data_bytes = 0;
    .var dpoffs = data_p_offs.sort()
    
    .var dpofi = 0;
    
    .while( fp < size + first_byte)
    {
      .var data_patched = false;
      .var in_data_range = false;
      
      .eval opc = file.uget(fp) // read byte
    
      .var curo = fp-first_byte
      
      
      .for(var dplp=0; dplp < data_patches.size(); dplp+=4)
      {
        .var dbeg = data_patches.get(dplp+0)
        .var dend = data_patches.get(dplp+1)
        .var orgbase = data_patches.get(dplp+2)
        .var newbase = data_patches.get(dplp+3)
        
        .if (curo >= dbeg && curo <= dend)
        {
          .eval in_data_range = true
          
          .eval fp = fp + 1 // advance file pointer
          
          .var cur_po = -1 
          .if (dpofi < dpoffs.size() )
            .eval cur_po = dpoffs.get(dpofi)
          
          .if(cur_po == curo) // patch it?
          {
            .eval patched_data_bytes = patched_data_bytes + 1
            .eval dpofi = dpofi + 1
            .var new_value = opc - orgbase + newbase;
            .if (d>1)
                .print "    found byte $" + toHexString(opc,2) + " at offset $" + toHexString(curo,4) + " patched: original_base_value $" + toHexString(orgbase,2) + " patch_base_value $" + toHexString(newbase,2) + " resulting in $" + toHexString(new_value,2)
            .eval opc = new_value
          }
        }
      }

      .byte opc // write opcode itself (or raw or patched data byte) in any case
      
      .if (!in_data_range) // check code patches 
      {
              .eval ops = asmCommandSize(opc)
              
              .if ( ops == 3) // all opcodes with absolute addressing occupy three bytes, the opcode itself plus the low-endian .word operand
              {
                .var low = file.uget(fp+1)
                .var high = file.uget(fp+2)
                
                .var operand = (high << 8) + low;
                
                .var lp = 0 // list pointer
                .var found = false
                .while(!found && lp < code_patches.size()) // check and process replacement lists 
                {
                  .var cbeg = code_patches.get(lp+0)
                  .var cend = code_patches.get(lp+1)
                  .var addb = code_patches.get(lp+2)
                  .var adde = code_patches.get(lp+3)
                  .var addt = code_patches.get(lp+4)
                  
                  .var vadd = fp-first_byte // instruction address relative to imported file range
                  
                  .if ( vadd >= cbeg && vadd <=cend && operand >= addb && operand <= adde) 
                  {
                    .eval found = true
                    .eval patched_addresses = patched_addresses + 1
                    
                    .if (d>1)
                      .print "    found absolute address $" + toHexString(operand,4) + " at relative offset $" + toHexString(vadd,4) + " opcode+bytes: $" + toHexString(opc,2) + " $" + toHexString(low,2) +" $" + toHexString(high,2) 
                    
                    .eval operand = operand - addb + addt // patch operand to new absolute target   
                    .eval low = operand & $ff
                    .eval high = (operand & $ff00) >> 8
                    
                    .if (d>1)
                      .print "       putting new address $" + toHexString(operand,4) + " at relative offset $" + toHexString(vadd,4) + " opcode+bytes: $" + toHexString(opc,2) + " $" + toHexString(low,2) +" $" + toHexString(high,2)
                    
                  }
                  .eval lp = lp + 5 // check next range in list
                }
                
                .byte low   // write (possibly modified) low byte
                .byte high  // write (possibly modified) high byte
              }
              
              .if (ops == 2)
                .byte file.uget(fp+1) // just copy the operand as is
              
              
              .if (ops <= 0) // safe-guard to always advance file pointer in case of bogus command size
              {
                 .eval ops = 1
              }
              .eval fp = fp + ops // advance file pointer
      }
    }
    
    .if (d>0)
    {
      .print "  imported '" + in_file + "' file bytes $"+toHexString(first_byte,4) + "-$" + toHexString(first_byte+size-1,4) + " to prg address $" + toHexString(import_address,4) + "-$" +toHexString(import_address+size-1,4)  
      .print "  in which " + toIntString(patched_addresses) + " absolute addresses have been patched."
      .print "  and also " + toIntString(patched_data_bytes) + " data bytes have been patched." 
    }
  }
  else
  {
    .error "code_patches must contain value quintets"
  }
}