// -----------------
// Interrupt Control
// -----------------
.label IRQ_VECTOR = $0314  // ..$0315 contains the 16bit user interrupt vector 

.var       NMI_HANDLER = $0000 // will be set later (replaces KERNAL $FE43) 
.var   IRQ_BRK_HANDLER = $0000 // will be set later (replaces KERNAL $FF48)
.var AFTER_IRQ_HANDLER = $0000 // will be set later (replaces KERNAL $EA81)

.label         NMI_PTR = $FFFA
.label     IRQ_BRK_PTR = $FFFE

// set the IRQ vector to the address of the given handler
.pseudocommand set_irq_vector handler_address
{ 
  sei
  :fetch_pointer IRQ_VECTOR : handler_address
  cli 
}

.label CIA1_IRQ_CTRL = $dc0d
.label CIA2_IRQ_CTRL = $dd0d

// set interrupt control to only trigger interrupts 
// by rasterline comparison
// handler_address = start of code which handles the interrupt
// first_line = raster line for the first time to trigger the interrupt
.pseudocommand init_irq_by_raster_only handler_address : first_line 
{
  sei // set interrupt disable
  
  lda #%01111111 
  sta CIA1_IRQ_CTRL // turn off timer interrupts from CIA #1
  sta CIA2_IRQ_CTRL // turn off timer interrupts from CIA #2
  lda CIA1_IRQ_CTRL // read once...
  lda CIA2_IRQ_CTRL // ...to cancel all queued/unprocessed CIA-IRQs
  :vic2_irq_by_raster_line_only // enable only raster line interrupts
  
  // set up first interrupt line
  :set_raster_compare first_line
  
  // wire up first interrupt handler
  :fetch_pointer IRQ_VECTOR : handler_address
  
  cli // clear interrupt disable
}

// sets handler address and raster line at which to trigger
// the next interrupt
// ( interrupts by rasterline must have been enabled for this to work )
.pseudocommand chain_irq_by_raster handler_address : line
{
  :set_raster_compare line
  :fetch_pointer IRQ_VECTOR : handler_address
}


// replacement for the nmi handler at $FE43 (pointer in $FFFA) when
// KERNAL ROM is switched out
.pseudocommand place_nmi_handler
{
  .if ( NMI_HANDLER == $0000)
  {
    .eval NMI_HANDLER = *
    .print getFilename() + ": NMI_HANDLER = " + address_of(NMI_HANDLER)
  
    sei
    // TODO:
    // should it become necessary for anything
    // temporarily switch KERNAL ROM back in here 
    // then jmp directly to $FE47 (which is the continuation of $FE43 
    // which just does sei and jmp ($0318) which by default lands at $FE47
    rti
  }
}


// replacement for the brk/irq handler at $FF48 (pointer in $FFFE) when 
// KERNAL ROM is switched out
//
.pseudocommand place_irq_brk_handler
{
  .if ( IRQ_BRK_HANDLER == $0000)
  {
    .eval IRQ_BRK_HANDLER = *
    .print getFilename() + ": IRQ_BRK_HANDLER = " + address_of(IRQ_BRK_HANDLER)
  
    :push_registers
  
    // snatched from disassembling KERNAL ROM, changed BRK behavior
    tsx
    lda $0104,x // read status from stack (NV-BDIZC)
    and #$10    // check break flag
    beq is_irq  // break not set ?
    // TODO: implement brk handling here
    is_irq:
    jmp ($0314) // next stop user IRQ handler
  }
}


// reaplacement for the normal after irq handler at $EA81 when
// KERNAL ROM is switched out
//
.pseudocommand place_end_irq_handler
{
  .if ( AFTER_IRQ_HANDLER == $0000 )
  {
    .eval AFTER_IRQ_HANDLER = *
    .print getFilename() + ": AFTER_IRQ_HANDLER = " + address_of(AFTER_IRQ_HANDLER)
    
    :pull_registers
    rti
  }
}


.pseudocommand begin_irq_handler { } 
// actually does nothing as it's already all handled by KERNAL 
// or cavelibs replacement for that 
// but it still looks good in code for clarity ;) 

// this on the other hand is mandatory to use at end of irq routines
.pseudocommand end_irq_handler { jmp AFTER_IRQ_HANDLER; }
