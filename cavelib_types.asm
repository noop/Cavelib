// -----------------------------------
// data types and associated functions
// -----------------------------------

// generates bytes for a C-style zero terminated string
.macro cstring(content)
{
  .text content
  .byte 0
}

// generates bytes for a string (in C64 screen code encoding)
// 
// the screen code bytes are pre-pended by a 16bit value(low,high) 
// containing the number of screen codes in the string which follows 
// because
// all values from 0 to 255 are valid screen codes
// so a 0 can not be used as a reliable terminator there
// 
// if inverted is 1, 128 is added to each screen code
.macro senc_string(inverted, ascii_in)
{
  .var to_convert = ascii_in
  .var invert_add = inverted == 0 ? 0 : 128

  // try to guess if a List of codes was passed instead of a string
  .var test_list = ""+ascii_in // convert in parameter to string representation
  // is of the form "[value,value,...]" if it was a List
  .var is_probably_list = test_list.charAt(0) == '[' && test_list.charAt(test_list.size()-1) == ']' && test_list.size() != ascii_in.size() ? 1 : 0

  // place string size 16bit value
  .var length = to_convert.size()
  .word length

  // place string
  .var i = 0
  .for(i=0; i<length; i++)
  {
    .if( is_probably_list == 0)
    {
      .byte [to_convert.charAt(i)+invert_add]
    }
    else
    {
      .byte [to_convert.get(i)+invert_add]
    }
  }
}

//.macro senc_string(inverted, 

.var subroutine_cstring_length = $0000

.pseudocommand place_subroutine_cstring_length
{
  .if ( subroutine_cstring_length == $0000 )
  {
    .eval subroutine_cstring_length = *
    .print getFilename() + ": subroutine_cstring_length = " + address_of(subroutine_cstring_length)

    ldy #$00 // init length
    sty cl_w1
    sty cl_w1+1


    loop:
    lda (cl_w0),y
    beq done

    // increase length
    inc cl_w1
    bne skip_high_len_inc
    inc cl_w1+1
    skip_high_len_inc:

    // forward pointer
    inc cl_w0
    bne skip_high_inc
    inc cl_w0+1
    skip_high_inc:
    jmp loop

    done:

    // length is in cl_w1 now

    rts
  }
}

// cstring_address: location of a zero terminated byte sequence
// length_out:      location where to store the 16bit result
// clobbers: cl_w0, cl_w1
.pseudocommand cstring_length cstring_address : length_out
{
  // make sure the subroutine was defined first
  .if ( subroutine_cstring_length == $0000 )
    .error ":cstring_length used without :place_subroutine_cstring_length"

  :fetch_pointer cl_w0 : cstring_address

  jsr subroutine_cstring_length

  lda cl_w1
  sta CmdArgument(AT_ABSOLUTE, length_out.getValue())
  lda cl_w1+1
  sta CmdArgument(AT_ABSOLUTE, length_out.getValue()+1)
}

