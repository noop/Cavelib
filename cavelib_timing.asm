// ----------------------------
// general purpose cycle waster
// ----------------------------

.var subroutine_waste_cycles_19plus = $0000
.var subroutine_waste_cycles_at_rts = $0000

.pseudocommand place_subroutine_waste_cycles_all
{
  // delay "84 - A cycles" routine adapted from:
  // http://codebase64.org/doku.php?id=base:delay by Zed Yago
  .if ( subroutine_waste_cycles_19plus == $0000 )
  {
    .align $0080 // align to page start or half page (needed for relative branching)
    .eval subroutine_waste_cycles_19plus = *
    .print getFilename() + ": subroutine_waste_cycles_19plus = " + address_of(subroutine_waste_cycles_19plus)

     // waste 84 - A cycles , a in [0,65]
     lsr // 2 cycles
     // carry = 1 if A was odd, 0 if even
     bcc waste1 // 2 or 3 cycles depending whether A was even or odd
     waste1:
     sta smod+1 // 4 cycles modifies argument below to skip NOPs
     clc        // 2 cycles
     // 10 or 11 cycles wasted so far
     smod:
     bcc *+10   // 3 cycles
     .fill 32, NOP // NOP field
     .eval subroutine_waste_cycles_at_rts = *
     rts        // 6 cycles

     // this subroutine wastes at least 19 cycles
  }
}

// cycles_to_waste must be in the inclusive interval [2,84]
// and will be treated as an immediate value
// clobbers: A (if wasting at least 27 cycles)
.pseudocommand waste_cycles cycles_to_waste
{
  .if(cycles_to_waste.getValue() < 2)
    .error ":waste_cycles can not waste less than 2 cycles"

  .if(cycles_to_waste.getValue() > 84)
    .error ":waste_cycles can not waste more than 84 cycles at once (due to relative branching distance trickery in subroutine)"

  .if(subroutine_waste_cycles_at_rts == $0000)
    .error ":waste_cycles used without :place_subroutine_waste_cycles_all"

  .if(cycles_to_waste.getValue() >= 27)
  {
    lda CmdArgument(AT_IMMEDIATE, 92-cycles_to_waste.getValue()) // 2 cycles
    jsr subroutine_waste_cycles_19plus // 6 cycles
    // will waste at least 19 plus 8 plus given amount of cycles
  }
  else
  .if(cycles_to_waste.getValue() >= 12)
  {
    .if(mod(cycles_to_waste.getValue(), 2) == 0) // if even
    {
      jsr [subroutine_waste_cycles_at_rts-[[cycles_to_waste.getValue()-12]/2]]
    }
    else // if odd and at least 13
    {
      bit $00 // 3 cycles
      .if(cycles_to_waste.getValue() == 13)
      {
        .fill 5, NOP
      }
      .if(cycles_to_waste.getValue() >= 15)
      {
        jsr [subroutine_waste_cycles_at_rts-[[cycles_to_waste.getValue()-15]/2]]
      }
    }
  }
  else
    // 11 or less cycles needs individual code
    // generated in place because jsr+rts already needs 12 cycles
  .if(cycles_to_waste.getValue() < 12 )
  {
    .var loop_roof = floor(cycles_to_waste.getValue()/2)

    .if(mod(cycles_to_waste.getValue(), 2) == 1)
    {
      bit $00 // 3 cycles
      .eval loop_roof--
    }

    .var loop_waste
    .for(loop_waste=0 ; loop_waste < loop_roof; loop_waste++)
    {
      nop
    }
  }
}
