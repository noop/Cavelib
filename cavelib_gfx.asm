// -------------------------------------------
// graphics related stuff not low-level enough
// to be in the vic2 file
// -------------------------------------------


// Clears a text screens memory to [char] and sets color RAM to [color]
//   bank must be 0 to 3
// screen must be 0 to 15
//
.pseudocommand clear_text_screen bank : screen : char : color
{
  :range_test(bank, 0, 3)
  :range_test(screen, 0, 15)

  :memset #VIC2_SCREEN(bank.getValue(), screen.getValue()) : #$03e8 : char
  :memset #VIC2_COLOR_RAM : #$03e8 : color
}

// Copy the character rom data to an arbitrary location.
// (assumes IO is currently on and will toggle it off/on around the copy process)
//
// target = the absolute target address where to write the $1000 bytes of the char rom 
//          (or the address of the 16bit pointer containing the target address)
//
// Clobbers: A, X, Y, cl_w0 
//
.pseudocommand copy_char_rom target
{
  // make sure the subroutine was defined first
  .if (subroutine_copy_char_rom == $0000 )
    .error ":copy_char_rom used without :place_subroutine_copy_char_rom"
    
  // prepare call
  :fetch_pointer cl_w0 : target
  
  jsr subroutine_copy_char_rom
}

.var subroutine_copy_char_rom = $0000

// Clobbers: A, X, Y, cl_w0,  
//
.pseudocommand place_subroutine_copy_char_rom
{
  .if (subroutine_copy_char_rom == $0000)
  {
    .eval subroutine_copy_char_rom = *
    .print getFilename() + ": subroutine_copy_char_rom = " + address_of(subroutine_copy_char_rom)
  
    // re-use zero-page vars with different names for clarity
    .var target = cl_w0
  
    :toggle_IO
    :memcpy cl_w0 : #CHAR_ROM : #CHAR_ROM_SIZE
    :toggle_IO
  }
}


// Output glyphs and colors of a double height font to an arbitrary screen.
//
// the font must contain 128 * 8 bytes: 64 * 8 containing the upper halves
//                                      64 * 8 containing the lower halves
// of the ASCII characters numbered 32(space) to 95(underscore): !"#$% .. XYZ[\]^_
//
// (and the font is expected to be located in the second half of the 256 characters
// charset)
//
// changing color mid-string: Since this command can only render 64 different
//   characters (use all UPPER CASE in string), any lower case letter from 
//   a to p will be interpreted as a command to change the color
//   (a black, b white, c red, d cyan, 
//    e purple, f green, g blue, h yellow,
//    i orange, j brown, k lightred, l darkgray, 
//    m mediumgray, n lightgreen, o lightblue, p lightgray) 
//
//
// column = offset from left border in number of chars
// row    = offset from top border in number of single-height char rows
// color  = what to write into the respective color RAM position for each glyph
// string(max 255 chars) = the absolute start address of a zero terminated string in immediate mode
//          (or the address of the 16bit pointer containing the actual address of the string)
// target_screen = the absolute start address of any valid text screen location
//
// Clobbers: A, X, Y, cl_w0 to cl_w3, cl_b0 to cl_b4
//
.pseudocommand textout1x2 column : row : color : string : target_screen
{
   // make sure the subroutine was defined first
  .if ( subroutine_textout1x2 == $0000 )
    .error ":textout1x2 used without :place_subroutine_textout1x2"

  // prepare call
  :fetch_pointer cl_w3 : string
  :fetch_pointer cl_w1 : target_screen
  :set_bits cl_b0 : column
  :set_bits cl_b1 : row
  :set_bits cl_b2 : color

  jsr subroutine_textout1x2
}


.var subroutine_textout1x2 = $0000

// Clobbers: A, X, Y, cl_w0 to cl_w3, cl_b0 to cl_b4
//
.pseudocommand place_subroutine_textout1x2
{
  .if ( subroutine_textout1x2 == $0000 )
  {
    .eval subroutine_textout1x2 = *
    .print getFilename() + ": subroutine_textout1x2 = " + address_of(subroutine_textout1x2)

    // re-use some zero-page vars with different names for clarity
    .var offset = cl_w0
    .var target_ram = cl_w1
    .var target_color_ram = cl_w2
    .var string = cl_w3

    .var column = cl_b0
    .var row    = cl_b1

    .var current_char = cl_b3
    .var current_glyph = cl_b4

    .var color = cl_b2


    lda #$00                              // Clear some variables (execpt
    sta offset                            // those already initialized by
    sta offset+1                          // the textout1x2 pseudocommand)
    sta current_char                      //
    ldx row                               // X is our loop control variable
    beq row_0

    loop:
    :add_words offset : #$28 : offset     // Add another 40 to the offset
    dex                                   // Decrement x and loop if not zero
    bne loop                              //

    row_0:
    :add_word_byte offset : column : offset        // Add column to offset
    :add_words target_ram : offset : target_ram    // Add offset to starting screen address
    :add_words #VIC2_COLOR_RAM : offset : target_color_ram  // Add offset to starting color address

    // --------------------------------------------------------------------------------
    next_char:

    ldy current_char                      // Get character at address + current_char
    lda (string),y
    
    beq end_of_string                     // zero? string ends
    
    tax
    cpx #$11                              // check if it's a to p 
    bpl go_ahead                          // >= 17 (not a to p)
    
    dex                                   // minus 1 because small a is 1 (but colors are 0 to 15)
    stx color                             // change color
    inc current_char
    jmp next_char                         // proceed to next char
    
    go_ahead:

    clc
    adc #[128-32]                         // get to the correct glyph in the font
                                          // (first 128 glyphs left blank for tilesets)
    sta current_glyph                     //

    ldy #$00                              // Clear Y before doing indirect-indexed addressing
    lda current_glyph                     // Load our character (top half)
    sta (target_ram), Y                   // Write character
    lda color                             //
    sta (target_color_ram), Y             // Write color

    :add_words target_ram : #$28 : target_ram              // Offsets go up by 40 to hit bottom half
    :add_words target_color_ram : #$28 : target_color_ram  //

    lda current_glyph                     // Add 64 to address (to get lower half)
    clc                                   //
    adc #$40                              //
    sta (target_ram), Y                   // Write character
    lda color                             //
    sta (target_color_ram), Y             // Write color

    inc current_char                      // Address offset++
    :sub_words target_ram : #$27 : target_ram              // Character offset -= 39
    :sub_words target_color_ram : #$27 : target_color_ram  // Color mem offset -= 39
    jmp next_char                         // Repeat until delimiter found
    
    end_of_string:
    rts
  }
}


// Output bytes of a double height font into sprites (three per sprite).
// (only lines 0 to 15 of each consecutive sprite will be overwritten
//  (the first 3*16 bytes) with glyph data and the remaining lines will be
//  cleared)
//
// the font must contain 128 * 8 bytes: 64 * 8 containing the upper halves
//                                      64 * 8 containing the lower halves
// of the ASCII characters numbered 32(space) to 95(underscore): !"#$% .. XYZ[\]^_
//
// string(max 255 chars) = the absolute start address of a zero terminated string
//          
// bank = the bank number where the target sprites are located
// sprite_address  = address of the first sprite that will be clobbered with glyphs
// charset_address = address of the first byte in the font
//
// Clobbers: A, X, Y, cl_w0 to cl_w3, cl_b0 to cl_b3
//
.pseudocommand textout1x2_to_sprites string : sprite_address : charset_address
{
  // make sure the subroutine was defined first
  .if ( subroutine_textout1x2_to_sprites == $0000 )
    .error ":textout1x2_to_sprites used without :place_subroutine_textout1x2_to_sprites"
  
  // prepare call
  :fetch_pointer cl_w0 : string
  :fetch_pointer cl_w1 : sprite_address
  :fetch_pointer cl_w2 : charset_address
  
  jsr subroutine_textout1x2_to_sprites
}


.var subroutine_textout1x2_to_sprites = $0000

// Clobbers: A, X, Y, cl_w0 to cl_w3, cl_b0 to cl_b3
//
.pseudocommand place_subroutine_textout1x2_to_sprites 
{
  .if ( subroutine_textout1x2_to_sprites == $0000 )
  {
    .eval subroutine_textout1x2_to_sprites = *
    .print getFilename() + ": subroutine_textout1x2_to_sprites = " + address_of(subroutine_textout1x2_to_sprites)

    // re-use some zero-page vars with different names for clarity
    .var string = cl_w0
    .var sprite_address = cl_w1
    .var charset_address = cl_w2

    .var source_byte = cl_w3

    .var current_char = cl_b0 
    
    .var y1 = cl_b1
    .var y2 = cl_b2
    .var track_mod = cl_b3
    
    // init some vars
    lda #$00
    sta current_char
    
    lda #$03
    sta track_mod
    
    next_char:
    ldy current_char                      // Get character at address + current_char
    lda (string),y

    bne not_end_of_string                 // zero? string ends
    jmp end_of_string                     // need far jmp here
    not_end_of_string:
 
    // A still holds ASCII charcode here
    sec
    sbc #$20          // get to the correct glyph in the font
    sta source_byte   // low byte serves as glyph number now
    ldx #$00
    stx source_byte+1 // clear high byte

    // get source_byte offset (glyph number * 8) for upper half of charset data
    asl
    rol source_byte+1
    asl 
    rol source_byte+1
    asl 
    rol source_byte+1
    sta source_byte   
    // (low byte, high byte) was transformed to glyph number * 8 now
    
    :add_words charset_address : source_byte : source_byte
    
    // write 8 bytes from upperhalf into sprite layout
    ldy #$00
    sty y1
    sty y2
    
    ldx #$08 // loop control
    next_byte_upper:
    ldy y1
    lda (source_byte),y
    iny // advance by 1 to get to next source byte
    sty y1
    ldy y2
    sta (sprite_address),y
    iny 
    iny 
    iny // advance by 3 to get to next sprite row in same column
    sty y2
    dex
    bne next_byte_upper
    
    // get source_byte offset for lower half ( 64 * 8 from current address)
    :add_words source_byte: #$0200 : source_byte
    
    // write 8 bytes from lowerhalf into sprite layout
    ldy #$00
    sty y1
    
    ldx #$08 // loop control
    next_byte_lower:
    ldy y1
    lda (source_byte),y
    iny // advance by 1 to get to next source byte
    sty y1
    ldy y2
    sta (sprite_address),y
    iny 
    iny 
    iny // advance by 3 to get to next sprite row in same column
    sty y2
    dex
    bne next_byte_lower
    
    // clear space below chars inside sprite
    // Y is still y2 here
    ldx #$10 // loop control
    lda #$00
    empty_space:
    sta (sprite_address),y
    iny
    dex
    bne empty_space
    
    // adjust sprite byte pointer to next character position
    //  sprites are 3 * 21 bytes plus one gap byte between them
    //  column0  column1  column3
    //  -------------------------
    //  byte  0  byte  1  byte  2
    //  .........................
    //  byte 60  byte 61  byte 62 
    //  byte GAP (byte 63)
    //  byte 0 (of next sprite)
    dec track_mod
    bne not_next
    // skip 62 to get to next sprite
    :add_words sprite_address : #$003e : sprite_address 
    lda #$03
    sta track_mod
    jmp done
    not_next: 
    // first or second in each group of three glyphs
    :add_words sprite_address : #$0001 : sprite_address
    done:
    
    inc current_char
    jmp next_char                         // Repeat until delimiter found
    end_of_string:
    rts
  }
}
 
