.importonce

// cavelib (aka CLOBBER)
//
// by NOOP (demo group founded in 2015)
//
// a collection of re-usable Kick Assembler bits and pieces for C64 
// programming
//
// license: see LICENSE.txt
//
// NOTE: Some but not all routines in Cave Lib contain self-modifying code
//       and use locations in zero page for parameters and local working
//       storage.
//
//       This is a design decision to favor execution speed and to reduce the
//       size of the generated code where possible.
//
//       So as a rule of thumb,
//       treat everything in this lib as NOT MULTI-THREADING SAFE
//       and do not use any commands which branch into real subroutines
//       again unless you know they already finished the previous execution.
//
//       (this is easily achieved if you keep your code single-threaded)
//
// EVERYTHING GETS CLOBBERED MORE OR LESS. DON'T EVER ASSUME THAT ANY OF THE
// ROUTINES WILL EVER STORE/RESTORE YOUR REGISTERS/STATUS FOR YOU IMPLICITLY.
//
// DUE TO SELF-MODIFYING CODE, SOME OF THE ROUTINES CAN'T BE RAN IN ROM.

.var cavelib_version = "n0.2d_dev0"
// as stuff might still get changed around,
// user code should always assert against a specific version
.macro assert_cavelib_version(v)
{
  .if ( cavelib_version != v )
    .error getFilename() + @": user code requires cavelib version \""+v+@"\" while this is \""+cavelib_version+@"\""
}

// Cavelib compile-time configuration
#if CL_NO_CBO_LOADER // can be set before #import "cavelib.asm" to exclude loader, otherwise it is included
  .var include_covert_bitops_loader = 0 // 0 off, 1 on
#else
  .var include_covert_bitops_loader = 1 // 0 off, 1 on
#endif

// 3rd party imports
// (whether any of these actually end up in the generated code depends on config above)
.import source "./loader/loader_config.ka"
.import source "./loader/loader.ka" // Covert Bitops Loadersystem V2.26ka
                                    // snatched from http://covertbitops.c64.org/


// Cavelib imports
.import source "cavelib_types.asm"
.import source "cavelib_timing.asm"
.import source "cavelib_cfg.asm"
.import source "cavelib_func.asm"
.import source "cavelib_import.asm"
.import source "cavelib_vars.asm"
.import source "cavelib_bits.asm"
.import source "cavelib_vic2.asm"
.import source "cavelib_ram.asm"
.import source "cavelib_irq.asm"
.import source "cavelib_file.asm"

.import source "cavelib_math.asm"
.import source "cavelib_gfx.asm"

// --------------
// Initialization 
// --------------

// init cavelib
// it is important to use this as the first command which runs 
// at the programs entry point
// it disables the BASIC/KERNAL ROM and thus allows using 
// zero page RAM $02 to $ff and the RAM at $A000 - $BFFF and $E000 - $FFF9
// (FFFA to FFFF are used by cavelib for replacement NMI/IRQ routines)
// and initializes any variables which were defined in zero page to 0
// (as long as the commands meant for delimiting those were used properly)
.pseudocommand init_cavelib
{
  .if ( cavelib_routines_ready == 0 )
  {
    .printnow "cavelib usage example:"
    .printnow ""
    .printnow ":begin_zp_user_vars"
    .printnow "  a_fast_ptr: .word 0"
    .printnow "  status: .byte 0"
    .printnow ":end_zp_user_vars "
    .printnow ""
    .printnow @".pc = $0801 \"Basic Upstart\""
    .printnow ":BasicUpstart(main)"
    .printnow ""
    .printnow ":begin_slow_ram_vars_and_routines"
    .printnow "  some_array: .fill $20, 0"
    .printnow @"  hello: :cstring(\"hello world!\")"
    .printnow ":end_slow_ram_vars_and_routines"
    .printnow ""
    .printnow "main:" 
    .printnow ":init_cavelib"
    .printnow "// now do stuff"
    
    .error "cavelib user must include cavelib and any custom vars before :init_cavelib" 
  }
  
  // set KERNAL routine replacements in RAM before switching it out
  // --------------------------------------------------------------
  sei // set interrupt disable
  
  lda #%01111111 
  sta CIA1_IRQ_CTRL // turn off timer interrupts from CIA #1
  sta CIA2_IRQ_CTRL // turn off timer interrupts from CIA #2
  lda CIA1_IRQ_CTRL // read once...
  lda CIA2_IRQ_CTRL // ...to cancel all queued/unprocessed CIA-IRQs
  
  :fetch_pointer NMI_PTR : #NMI_HANDLER          // replace kernal NMI handler
  :fetch_pointer IRQ_BRK_PTR : #IRQ_BRK_HANDLER  // replace kernal IRQ/BRK handler
  :fetch_pointer IRQ_VECTOR : #AFTER_IRQ_HANDLER // replace kernal default IRQ handler
  
  :switch_out_ROM_BASIC_KERNAL
  :save_BASIC_zeropage
  :save_KERNAL_zeropage
  
  cli // resume interrupts
  
  :init_cavelib_vars
  :init_cavelib_user_vars
  
  :disable_runstop_check
}

// exits cavelib
// restores BASIC/KERNAL ROM (and basic/kernal working area in zero page)
// and breaks out of the running program
// mode = #0 to break / #1 to soft reset
.pseudocommand shutdown_cavelib mode
{
  :restore_KERNAL_zeropage
  :restore_BASIC_zeropage
  :switch_in_ROM_BASIC_KERNAL
  
  sei
  // restore IRQ handlers
  :fetch_pointer NMI_PTR : $FE43  
  :fetch_pointer IRQ_BRK_PTR : $FF48
  :fetch_pointer IRQ_VECTOR : $EA31
  
  :enable_runstop_check
  
  cli
  
  lda mode
  bne reset
  brk
  reset:
  :blank_screen
  jmp ($FFFC) // soft reset vector
}
