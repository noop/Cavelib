// -----------------------------
// Subroutine / Function helpers
// -----------------------------

// save all registers on stack
.pseudocommand push_registers { pha; txa; pha; tya; pha; } 

// save CPU status on stack
.pseudocommand push_status { php; }

// restore all registers from stack
.pseudocommand pull_registers { pla; tay; pla; tax; pla; }

// restore CPU status from stack
.pseudocommand pull_status { plp; }

// calls a subroutine without saving/restoring CPU status or registers
.pseudocommand call_dirty address { jsr address; }

// calls a subroutine at address and saves/restores CPU status and registers 
// to/from stack 
.pseudocommand call_safe address
{
  :push_status
  :push_registers
  jsr address
  :pull_registers
  :pull_status
}



// -----------------------
// compile time info stuff
// -----------------------
.function address_of(symbol)
{
  .return "$" + toHexString(symbol,4)
}

.macro range_test(value, min_value, max_value)
{
  .if (value.getValue() < min_value || value.getValue() > max_value)
    .error "value is out of range " + toIntString(min_value) + " to " + toIntString(max_value)
}

.function check_param(param, value, min_value, max_value)
{
  .if (value < min_value || value > max_value)
    .error param + "(" + toIntString(value) + ") out of range " + toIntString(min_value) + " to " + toIntString(max_value)
}

// Enables creation of breakpoints
//
.var brk_file = createFile("breakpoints.txt")

.macro break() 
{
  .eval brk_file.writeln("break " + toHexString(*))
}

.macro break_at(address)
{
  .eval brk_file.writeln("break " + toHexString(address))
}