// Cavelib
// example 02 - hello world with a progress bar


#import "cavelib.asm"

.const tf = 158 // total frames
.const pc = '.' // progress character


:begin_zp_user_vars
  frames: .word tf // frame counter (caveat: initialization like this is not supported for zero page vars!)
  pscreen: .word 0 // pointer to a screen position
:end_zp_user_vars 
   


.pc = $0801 "Basic Upstart"
:BasicUpstart(main)
  


:begin_slow_ram_vars_and_routines
  hello: :senc_string(0, "hello world!") // encode string in char codes, first two bytes will be the length in bytes
:end_slow_ram_vars_and_routines
   


main:
  :init_cavelib  
  // asserting a specific version at assembly time is optional
  // but recommended as future changes to cavelib might
  // break backward compatibility
  assert_cavelib_version("n0.2d_dev0")

  // init zero-page vars for real now
  lda #<tf
  sta frames
  lda #>tf
  sta frames+1
  
  lda #<VIC2_SCREEN(0,1)+1
  sta pscreen
  lda #>VIC2_SCREEN(0,1)+1
  sta pscreen+1

  // clear text screen in bank 0, screen number 1, with space char, write white to color ram
  :clear_text_screen #0 : #1 : #' ' : #WHITE
  // place the hello string at center of line 12 in bank 0, screen number 1
  :memcpy #VIC2_SCREEN(0,1)+12*40+14 : #hello+2 : hello
  
  lda #'['
  sta VIC2_SCREEN(0,1)
  lda #']'
  sta VIC2_SCREEN(0,1)+tf+1
  
  
  :init_irq_by_raster_only #at_vblank : #PAL_VBLANK_START 

  main_loop:
    nop
    
    // countdown reached 0 ?
    ldy frames+1
    cpy #0
    bne main_loop
    ldx frames
    cpx #0
    bne main_loop
    
    :disable_raster_compare_irq
    
  end_main:
  :shutdown_cavelib #0
  
  
  
at_vblank:
    :begin_irq_handler
    
    ldx frames
    stx VIC2_BORDER_COLOR
    
    // frame countdown logic
    :sub_word_byte frames : #1 : frames
    
    low_only:
       
    // draw progress dot to screen pointer
    lda #'.'
    ldy #0
    sta (pscreen),y
    
    // advance screen pointer by 1
    :add_words pscreen : #1 : pscreen
    
    :ack_vic2_irq_all
    :end_irq_handler