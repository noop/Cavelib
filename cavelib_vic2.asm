// ------------------------
// Screen Constants
// ------------------------
.const PAL_VBLANK_START = 300 // first line after lower border
.const PAL_VBLANK_END = 15    // last line before upper border
.const PAL_INNER_FIRST_LINE = 51 // first line of inner screen area
.const PAL_INNER_LAST_LINE = 250 // last line of inner screen area 
.const PAL_INNER_FIRST_SP_COLUMN = 24


// ------------------------
// VIC-II Control Registers
// ------------------------
.label VIC2_SP0X = $d000; .label VIC2_SP0Y = $d001 
.label VIC2_SP1X = $d002; .label VIC2_SP1Y = $d003
.label VIC2_SP2X = $d004; .label VIC2_SP2Y = $d005
.label VIC2_SP3X = $d006; .label VIC2_SP3Y = $d007
.label VIC2_SP4X = $d008; .label VIC2_SP4Y = $d009
.label VIC2_SP5X = $d00a; .label VIC2_SP5Y = $d00b
.label VIC2_SP6X = $d00c; .label VIC2_SP6Y = $d00d
.label VIC2_SP7X = $d00e; .label VIC2_SP7Y = $d00f
.label VIC2_SPHX = $d010
.label VIC2_CTRL1 = $d011
.label VIC2_RASTER_COMPARE_LOW = $d012
.label VIC2_LPEN_X = $d013 // 0 to 160 (*2 gives approx. position of lightpen) 
.label VIC2_LPEN_Y = $d014 // current raster line of the light pen
.label VIC2_SPRITE_ENABLED_FLAGS = $d015
.label VIC2_CTRL2 = $d016
.label VIC2_SPRITE_STRETCH_Y = $d017 
 
.label VIC2_MEM_CTRL = $d018 // controls location of charset/bitmap/color
// NOTE: charset/bitmap location are controlled by the same bits
//       but for the bitmap location only the most significant of those
//       bits is relevant (bitmap can be in first or second half of the bank)

.label VIC2_IRQ_FLAGS = $d019
.label VIC2_IRQ_MASK = $d01a
.label VIC2_SPRITE_BEHIND_FOREGROUND = $d01b
.label VIC2_SPRITE_MODE = $d01c
.label VIC2_SPRITE_STRETCH_X = $d01d
.label VIC2_SPRITE_SPRITE_COLLISION = $d01e     // (gets cleared on read)
.label VIC2_SPRITE_FOREGROUND_COLLISION = $d01f // (gets cleared on read)

.label VIC2_BORDER_COLOR = $d020         
.label VIC2_BACKGROUND_COLOR = $d021     
.label VIC2_BACKGROUND_COLOR0 = $d021  
.label VIC2_BACKGROUND_COLOR1 = $d022 
.label VIC2_BACKGROUND_COLOR2 = $d023 
.label VIC2_BACKGROUND_COLOR3 = $d024 

.label VIC2_SPRITE_SHARED_COLOR0 = $d025
.label VIC2_SPRITE_SHARED_COLOR1 = $d026
.label VIC2_SP0COLOR = $d027
.label VIC2_SP1COLOR = $d028
.label VIC2_SP2COLOR = $d029
.label VIC2_SP3COLOR = $d02a
.label VIC2_SP4COLOR = $d02b
.label VIC2_SP5COLOR = $d02c
.label VIC2_SP6COLOR = $d02d
.label VIC2_SP7COLOR = $d02e
 
.label VIC2_COLOR_RAM = $d800


.label CIA2_DATA_PORT_A_DIR = $dd02 // (1 output, 0 input) direction for corresponding bit in port A
.label CIA2_DATA_PORT_A = $dd00 // bit 0-1 are for selecting VIC memory bank 
// VIC-II banks
// bank_start = 0 + bank * 16384
// bank0 = 0000 to 3fff (VIC-II sees char ROM at $1000-$1fff)
// bank1 = 4000 to 7fff
// bank2 = 8000 to bfff (VIC-II sees char ROM at $9000-$9fff)
// bank3 = c000 to ffff
.label VIC2_BANK0 = $0000
.label VIC2_BANK1 = $4000 // (most usable bank as there is nothing in here by default)
.label VIC2_BANK2 = $8000 // (contains BASIC ROM unless switched out)
.label VIC2_BANK3 = $c000 // (contains KERNAL ROM unless switched out)


// get absolute RAM location for a specific sprite pointer
// (one of the eight sprite pointers at the end of each screen)
.function VIC2_SPP(bank_no, screen_no, sprite_no)
{
  .eval check_param("bank_no", bank_no, 0, 4)
  .eval check_param("screen_no", screen_no, 0, 15)
  .eval check_param("sprite_no", sprite_no, 0, 7)

  .return [$4000 * bank_no + $0400 * screen_no + $0400 - [$08-sprite_no]]
}

.function VIC2_SPDATA(bank_no, sprite_def_num)
{
  .eval check_param("bank_no", bank_no, 0, 4)
  .eval check_param("sprite_def_num", sprite_def_num, 0, 255)
  
  .return [$4000 * bank_no + $0040 * sprite_def_num]
}

.function VIC2_BANK(bank_no)
{
  .eval check_param("bank_no", bank_no, 0, 4)

  .return [$4000 * bank_no] 
}

.function VIC2_SCREEN(bank_no, screen_no)
{
  .eval check_param("bank_no", bank_no, 0, 4)
  .eval check_param("screen_no", screen_no, 0, 15)

  .return [$4000 * bank_no + $0400 * screen_no]
}

.function VIC2_CHARSET(bank_no, charset_no)
{
  .eval check_param("bank_no", bank_no, 0, 4)
  .eval check_param("charset_no", charset_no, 0, 7)

  .return [$4000 * bank_no + $0800 * charset_no]
}

.function VIC2_BITMAP(bank_no, bitmap_no)
{
  .eval check_param("bank_no", bank_no, 0, 4)
  .eval check_param("bitmap_no", bitmap_no, 0, 1)

  .return [$4000 * bank_no + $2000 * bitmap_no]
}


// --------------------------------------
// VIC-II Control Register 1 manipulators
// --------------------------------------

// note that writing to this control register will always also cause
// (even when the previous value is read and none of the other bits are touched)
// the raster compare high bit to be reset to whatever the currently drawn
// rasterline implies ( this is because on setting this bit it will be stored
// somewhere else in the hardware for the interrupt trigger but afterwards
// the bit in the register will immediately be set to 0 or 1 depending whether
// the currently drawn line is < 256 or >= 256 ) so writing to this register
// will in almost any case cause any scheduled interrupt-by-specific-line 
// request to be reset to trigger at the wrong line 
// (in other words: after using this register, make sure to set the raster 
//  compare high bit to 1 once more if the line where to interrupt should 
//  be >= 256 )

// soft scroll along vertical axis: 0 to 7 lines
// (lines  will not be sanity checked, values above 7 can disturb vic regsiter)
.pseudocommand soft_scroll_y lines { lda VIC2_CTRL1; and #$F8; ora lines; sta VIC2_CTRL1; } 

// set 24/25 row modes 
.pseudocommand set_24_rows { :clear_bit3 VIC2_CTRL1; } 
.pseudocommand set_25_rows {   :set_bit3 VIC2_CTRL1; }

// blank/show screen (blanks screen to border color "hides" it)
.pseudocommand blank_screen { :clear_bit4 VIC2_CTRL1; }
.pseudocommand show_screen  {   :set_bit4 VIC2_CTRL1; }

// activate/deactivate bitmap mode
.pseudocommand bitmap_mode_on  {   :set_bit5 VIC2_CTRL1; }
.pseudocommand bitmap_mode_off { :clear_bit5 VIC2_CTRL1; } 

// activate/deactivate extended color text mode
.pseudocommand extended_color_text_mode_on  {   :set_bit6 VIC2_CTRL1; }
.pseudocommand extended_color_text_mode_off { :clear_bit6 VIC2_CTRL1; }

// set/clear/test raster compare high bit (bit "8" for d012)
// after testing processor status N flag will contain the bit value
//  so BMI would branch afterwards if the rasterline was  > 255
// and BPL would branch afterwards if the rasterline was <= 255 
.pseudocommand set_raster_compare_high   {   :set_bit7 VIC2_CTRL1; }
.pseudocommand clear_raster_compare_high { :clear_bit7 VIC2_CTRL1; }
.pseudocommand test_raster_compare_high  {         bit VIC2_CTRL1; }

// set/get raster IRQ line (0 to 255) (256 and above need extra bit in d011)
// (NTSC has 262 lines | PAL has 312 lines )
// (so code compatible with both should ideally not use more than 262 lines worth of "rastertime"
//  or split logic updates across several frames)
// bit 8 for the raster line is the 7th bit in control register 1 (see above)
// and must be set to generate a raster interrupt for line numbers beyond 255

// value_source = any standard 6510 assembly address expression
.pseudocommand set_raster_compare_low value_source { :set_bits VIC2_RASTER_COMPARE_LOW : value_source; }

// set the full raster compare line from 16 bit value
// (slower because it has to compare the high byte to determine
//  whether to set the high bit in the VIC2 register for lines > 255)
.pseudocommand set_raster_compare line16b
{
  :clear_raster_compare_high
  lda get_high(line16b)
  beq set_high_skipped
  :set_raster_compare_high
  set_high_skipped:
  :set_raster_compare_low get_low(line16b)
}

// --------------------------------------
// VIC-II Control Register 2 manipulators
// --------------------------------------

// soft scroll along horizontal axis: 0 to 7 columns
// (columns will not be sanity checked, values above 7 can disturb vic register)
.pseudocommand soft_scroll_x columns { lda VIC2_CTRL2; and #$F8; ora columns; sta VIC2_CTRL2; }

// set 38/40 column modes 
.pseudocommand set_38_columns { :clear_bit3 VIC2_CTRL2; } 
.pseudocommand set_40_columns {   :set_bit3 VIC2_CTRL2; }

// activate/deactivate multicolor mode
.pseudocommand multicolor_mode_on  {   :set_bit4 VIC2_CTRL2; }
.pseudocommand multicolor_mode_off { :clear_bit4 VIC2_CTRL2; }

// turn video chip off/on 
// (does not seem to work... maybe only on older hardware models)
.pseudocommand vic2_chip_on  { :clear_bit5 VIC2_CTRL2; }
.pseudocommand vic2_chip_off {   :set_bit5 VIC2_CTRL2; }


// ------------------------------------------------------------
// Border and Background Color settings and fixed Color RAM
// (fixed color RAM is used in all modes except in HiRes Bitmap
// ------------------------------------------------------------

// value_source = any standard 6510 assembly address expression 
// or one of these constant expressions 
// VIC_BLACK     00         
// #WHITE        01 
// #RED          02
// #CYAN         03
// #PURPLE       04
// #GREEN        05
// #BLUE         06
// #YELLOW       07
// #ORANGE       08
// #BROWN        09
// #LIGHT_RED    10
// #DARK_GR*Y    11 * = A or E
// #GR*Y         12
// #LIGHT_GREEN  13
// #LIGHT_BLUE   14
// #LIGHT_GR*Y   15
.pseudocommand set_border_color value_source      { :set_bits VIC2_BORDER_COLOR : value_source; } 
.pseudocommand set_background_color value_source  { :set_bits VIC2_BACKGROUND_COLOR0 : value_source; }
.pseudocommand set_background_color0 value_source  { :set_bits VIC2_BACKGROUND_COLOR0 : value_source; }
.pseudocommand set_background_color1 value_source  { :set_bits VIC2_BACKGROUND_COLOR1 : value_source; }
.pseudocommand set_background_color2 value_source  { :set_bits VIC2_BACKGROUND_COLOR2 : value_source; }
.pseudocommand set_background_color3 value_source  { :set_bits VIC2_BACKGROUND_COLOR3 : value_source; }
.pseudocommand set_global_color value_source { lda value_source; sta VIC2_BORDER_COLOR; sta VIC2_BACKGROUND_COLOR; }  


// ------------------------
// Hardware Sprites Control
// ------------------------

// enable/disable sprites 
// e.g. :set_sprites_enabled #%11000000
//       to enable the 1st 7th and 8th sprite and disable others
.pseudocommand set_sprites_enabled pattern { :set_bits VIC2_SPRITE_ENABLED_FLAGS : pattern; }

// display sprites behind(1) or in front(0 default) of foreground graphics
.pseudocommand set_sprites_behind_foreground pattern { :set_bits VIC2_SPRITE_BEHIND_FOREGROUND : pattern; }

// set individual sprite modes to multicolor(1) or hi-res single color (0)
.pseudocommand set_sprites_mode pattern { :set_bits VIC2_SPRITE_MODE : pattern; }

// offset sprites by 0 or 256 pixels along the x axis relative to upper left corner of screen
// pattern: %00000000 (0 offset = 0 | 1 offset = 256)
//           76543210 <-- sprite numbers corresponding to the bits
.pseudocommand set_sprites_x_high pattern { :set_bits VIC2_SPHX : pattern; } 
// set single/double width sprite flags
.pseudocommand set_sprites_stretch_x pattern { :set_bits VIC2_SPRITE_STRETCH_X : pattern; }
// set single/double height sprite flags
.pseudocommand set_sprites_stretch_y pattern { :set_bits VIC2_SPRITE_STRETCH_Y : pattern; }

// set a sprites x position low byte only (0 to 255)
.pseudocommand set_sprite_x_low sprite_id : pos_x 
{ 
  :range_test(sprite_id, 0, 7)
  
  .var address = sprite_id.getValue()*2 + VIC2_SP0X
  //.print "sprite number "+toIntString(sprite_id.getValue()) + " x(low) is at $"+toHexString(address,4)
  :set_bits CmdArgument(AT_ABSOLUTE, address) : pos_x
}

// set a sprites full x position low byte and high bit offset
// based on a 16bit value known at compile time
.pseudocommand set_sprite_x sprite_id : pos_x_full
{
  :range_test(sprite_id, 0, 7)
  
  :set_sprite_x_low sprite_id : pos_x_full
  .var bitpattern = 1 << sprite_id.getValue()
  .var bitpattern_inverted = bitpattern ^ $ff
  .if (pos_x_full.getValue() > 255)
  {
    :or_bits VIC2_SPHX : CmdArgument(AT_IMMEDIATE, bitpattern)
  }
  else
  {
    :and_bits VIC2_SPHX : CmdArgument(AT_IMMEDIATE, bitpattern_inverted)
  }
}

// set a sprites y position
.pseudocommand set_sprite_y sprite_id : pos_y
{
  :range_test(sprite_id, 0, 7)
  
  .var address = sprite_id.getValue()*2 + VIC2_SP0Y 
  //.print "sprite number "+toIntString(sprite_id.getValue()) + " y is at $"+toHexString(address,4)
  :set_bits CmdArgument(AT_ABSOLUTE, address) : pos_y
} 

// set sprite colors
.pseudocommand set_sp0color value_source { :set_bits VIC2_SP0COLOR : value_source; }
.pseudocommand set_sp1color value_source { :set_bits VIC2_SP1COLOR : value_source; }
.pseudocommand set_sp2color value_source { :set_bits VIC2_SP2COLOR : value_source; }
.pseudocommand set_sp3color value_source { :set_bits VIC2_SP3COLOR : value_source; }
.pseudocommand set_sp4color value_source { :set_bits VIC2_SP4COLOR : value_source; }
.pseudocommand set_sp5color value_source { :set_bits VIC2_SP5COLOR : value_source; }
.pseudocommand set_sp6color value_source { :set_bits VIC2_SP6COLOR : value_source; }
.pseudocommand set_sp7color value_source { :set_bits VIC2_SP7COLOR : value_source; }
.pseudocommand set_sprite_shared_color0 value_source { :set_bits VIC2_SPRITE_SHARED_COLOR0 : value_source; }
.pseudocommand set_sprite_shared_color1 value_source { :set_bits VIC2_SPRITE_SHARED_COLOR1 : value_source; }


// ---------------------------------
// VIC2 Memory Configuration Control
// ---------------------------------

// select a memory bank_start address for the VIC chip
// bank_start = 0 + bank * 16384
// bank0 = 0000 to 3fff (VIC-II sees char ROM at $1000-$1fff)
// bank1 = 4000 to 7fff
// bank2 = 8000 to bfff (VIC-II sees char ROM at $9000-$9fff)
// bank3 = c000 to ffff
.pseudocommand select_video_bank bank_number
{
  :range_test(bank_number, 0, 3)
  
  // invert bank_number bits because 11 is bank 0
  .var value = CmdArgument(AT_IMMEDIATE, bank_number.getValue() ^ $03)
  
  // set bits 0 and 1 to output
  lda CIA2_DATA_PORT_A_DIR
  ora #%00000011
  sta CIA2_DATA_PORT_A_DIR
  
  // set bank
  lda CIA2_DATA_PORT_A
  and #%11111100
  ora value
  sta CIA2_DATA_PORT_A
}

// sets the base address for the character data in multiples of 2KB
// as an offset from the bank_start address
// character-dot-data will be read from
// bank_start + k2_multiple * 2048
.pseudocommand set_charset_number k2_multiple 
{
  :range_test(k2_multiple, 0, 7)

  lda VIC2_MEM_CTRL
  and #%11110001
  ora CmdArgument(AT_IMMEDIATE, [k2_multiple.getValue() << 1])
  sta VIC2_MEM_CTRL
}
// same as above but with supplying an absolute offset
// (must be a multiple of $0800)
.pseudocommand set_charset_offset_absolute uint_16
{
  //.print toHexString(uint_16.getValue() >> 11)
  :set_charset_number CmdArgument(AT_IMMEDIATE, [uint_16.getValue() >> 11])
}

// sets the base address for the bitmap data in multiples of 8KB
// as an offset from the bank_start address
// bitmap-dot-data will be read from 
// bank_start + k8_multiple * 8192 
// the bitmap can only be located in the first or second half of the bank
.pseudocommand set_bitmap_number k8_multiple
{
  :range_test(k8_multiple, 0, 1)

  lda VIC2_MEM_CTRL
  and #%11110001
  ora CmdArgument(AT_IMMEDIATE, [k8_multiple.getValue() << 3])
  sta VIC2_MEM_CTRL
}
// same as above but with supplying an absolute offset
// (must be $0000 or $2000)
.pseudocommand set_bitmap_data_offset_absolute uint_16
{
  //.print toHexString(uint_16.getValue() >> 13)
  :set_bitmap_number CmdArgument(AT_IMMEDIATE, [uint_16.getValue() >> 13])
}

// sets the base address for the screen memory in multiples of 1KB 
// as an offset from the bank_start address
// screen memory will be read from 
// bank_start + k_multiple * 1024 
.pseudocommand set_screen_number k_multiple
{
  :range_test(k_multiple, 0, 15)
  
  lda VIC2_MEM_CTRL
  and #%00001111
  ora CmdArgument(AT_IMMEDIATE, [k_multiple.getValue() << 4])
  sta VIC2_MEM_CTRL
}
// same as above but with supplying an absolute offset
// (must be a multiple of $0400)
.pseudocommand set_screen_memory_offset_absolute uint_16
{
  //.print toHexString(uint_16.getValue() >> 10)
  :set_screen_number CmdArgument(AT_IMMEDIATE, [uint_16.getValue() >> 10])
}


// --------------------------
// VIC2 IRQ Query and Control
// --------------------------

// query the interrupt flags register
// follow with BNE or BEQ
// BNE will branch if the interrupt is of the tested type
// BEQ will branch if the interrupt is not of the tested type

// interrupt caused by raster line comparison? 
.pseudocommand test_irq_source_raster_compare { :test_bit0 VIC2_IRQ_FLAGS; }
// interrupt caused by any sprite colliding with foreground graphics?
.pseudocommand test_irq_source_collision_sprite_foreground { :test_bit1 VIC2_IRQ_FLAGS; }
// interrupt caused by any sprite colliding with any other sprite? 
.pseudocommand test_irq_source_collision_sprite_sprite { :test_bit2 VIC2_IRQ_FLAGS; }
// interrupt caused by light pen?
.pseudocommand test_irq_source_light_pen { :test_bit3 VIC2_IRQ_FLAGS; }
// interrupt caused by any of the above sources at all?
.pseudocommand test_irq_source_vic2_any { :test_bit7 VIC2_IRQ_FLAGS; }

// acknowledge interrupts (all at once)
.pseudocommand ack_vic2_irq_dirty { asl VIC2_IRQ_FLAGS; }
.pseudocommand ack_vic2_irq_all { lda #%00001111; sta VIC2_IRQ_FLAGS; }

// enable/disable interrupt requests for different sources
.pseudocommand enable_raster_compare_irq    {  :set_bit0 VIC2_IRQ_MASK; }
.pseudocommand disable_raster_compare_irq   {:clear_bit0 VIC2_IRQ_MASK; }
.pseudocommand vic2_irq_by_raster_line_only {:set_bits VIC2_IRQ_MASK : #%00000001; }

.pseudocommand enable_collision_sprite_foreground_irq  {  :set_bit1 VIC2_IRQ_MASK; }
.pseudocommand disable_collision_sprite_foreground_irq {:clear_bit1 VIC2_IRQ_MASK; }

.pseudocommand enable_collision_sprite_sprite_irq  {  :set_bit2 VIC2_IRQ_MASK; }
.pseudocommand disable_collision_sprite_sprite_irq {:clear_bit2 VIC2_IRQ_MASK; }

.pseudocommand enable_light_pen_irq  {  :set_bit3 VIC2_IRQ_MASK; }
.pseudocommand disable_light_pen_irq {:clear_bit3 VIC2_IRQ_MASK; }
