// TODO: organize stuff from TEST AREA into separate file(s) 
//
// new stuff TEST AREA begin
// ------------------------------------------------------------------------



// new stuff TEST AREA end
// ------------------------------------------------------------------------
// ------------------------------------------------------------------------
// ------------------------------------------------------------------------
// ------------------------------------------------------------------------



// --------------------
// Compile Time Control
// --------------------
.var cavelib_basic_zp_saved  = $0000 // will be set in cavelib_vars.asm 
.var cavelib_kernal_zp_saved = $0000 // will be set in cavelib_vars.asm

// ---------------------------------
// Runtime Environment Configuration
// ---------------------------------

// -----------------
// ROM/RAM/IO select
// -----------------
.label D6510 = $00 // 6510 on-chip I/O DATA Direction Register
.label R6510 = $01 // ROM/RAM selection and Cassette I/O

// -----------------------
// ROM locations and sizes
// -----------------------
.label    BASIC_ROM = $A000
.var BASIC_ROM_SIZE = $2000

.label    CHAR_ROM = $D000
.var CHAR_ROM_SIZE = $1000

.label    KERNAL_ROM = $E000
.var KERNAL_ROM_SIZE = $2000


// disables the BASIC ROM at $A000 - $BFFF 
//         and KERNAL ROM at $E000 - $FFFF 
// 
// also allowes using zero page $02 - $FF which are only used by BASIC/KERNAL
.pseudocommand switch_out_ROM_BASIC_KERNAL {   :set_bit0 R6510   // R6510 =
                                             :clear_bit1 R6510; } // #%..01
// revert the above command (zero page is NOT restored automatically)
.pseudocommand switch_in_ROM_BASIC_KERNAL { :or_bits R6510 : #%00000011; }

// toggle KERNAL on/off (BASIC stays off in any case)
// zero page stuff from $90-$FF is not automatically saved/restored
.pseudocommand toggle_KERNAL { :eor_bits R6510 : #%00000011; }

// toggle IO REGISTERS on/off (sloppily assuming the two lowest bits are never 00)
.pseudocommand toggle_IO { :eor_bits R6510 : #%00000100; }

// switch in the character ROM data to be visible for the main CPU at  
// $D000 - $DFFF  
// NOTE: $D000 - $DFFF by default is used to control I/O for SID, VIC-II and  
// for both CIA chips, so while this ROM is active, no I/O can be performed  
.pseudocommand switch_in_ROM_CHAR { :clear_bit2 R6510; }

// revert the above command
.pseudocommand switch_out_ROM_CHAR { :set_bit2 R6510; }

// save/restore BASIC zeropage working storage area to/from slow RAM
.pseudocommand save_BASIC_zeropage    { :memcpy #cavelib_basic_zp_saved : #$0002 : #$8e; }
.pseudocommand restore_BASIC_zeropage { :memcpy #$0002 : #cavelib_basic_zp_saved : #$8e; }

// save/restore KERNAL zeropage working storage area to/from slow RAM
.pseudocommand save_KERNAL_zeropage    { :memcpy #cavelib_kernal_zp_saved : #$0090 : #$70; }
.pseudocommand restore_KERNAL_zeropage { :memcpy #$0090 : #cavelib_kernal_zp_saved : #$70; }

// -----------------------------
// KERNAL behavior modifications 
// -----------------------------

// address of "query stop key indicator" routine
.var kernal_stop_vector = $0328 // jump vector (reroute to disable stop key indicator check)
.var sm_kernal_stop_org_low = $0000
.var sm_kernal_stop_org_high = $0000
.var subroutine_disable_runstop_check = $0000
.var subroutine_enable_runstop_check = $0000
.var subroutine_runstop_check_replacement = $0000

// rewires the kernal vector for the runstop-key-pressed-check to effectively
// have this check always report "not pressed"
// (useful to disable cancelling of a synchronous kernal LOAD by user input)
.pseudocommand disable_runstop_check
{
  .if ( subroutine_disable_runstop_check == $0000 ) // not defined yet?
  { //break()
    jsr start
    jmp behind // so that we're not trapped endlessly after first call
  
    // begin of "subroutine_runstop_check_replacement"
    .eval subroutine_runstop_check_replacement = *
    .print getFilename() + ": subroutine_runstop_check_replacement = " + address_of(subroutine_runstop_check_replacement)
    clc      // carry 0 "not pressed"
    lda #$FF // causes z flag = 0 (interpreted as "not pressed")
    rts
    // end of "subroutine_runstop_check_replacement"
  
    .eval sm_kernal_stop_org_low = *
    nop // placeholder for self-modifying storage of original vectors low byte
    .eval sm_kernal_stop_org_high = *
    nop // placeholder for self-modifying storage of original vectors high byte
  
    // begin of "subroutine_enable_runstop_check"
    .eval subroutine_enable_runstop_check = *
    .print getFilename() + ": subroutine_enable_runstop_check = " + address_of(subroutine_enable_runstop_check)
    sei
    lda sm_kernal_stop_org_low
    sta kernal_stop_vector
    lda sm_kernal_stop_org_high
    sta kernal_stop_vector+1
    cli
    // original runstop check vector now restored from the "nop" bytes above 
    rts
    // end of "subroutine_enable_runstop_check"
    
    start:
    // begin of "subroutine_disable_runstop_check"
    .eval subroutine_disable_runstop_check = *
    .print getFilename() + ": subroutine_disable_runstop_check = " + address_of(subroutine_disable_runstop_check)
    sei
    lda kernal_stop_vector
    sta sm_kernal_stop_org_low
    lda kernal_stop_vector+1
    sta sm_kernal_stop_org_high
    // original runstop check vector now stored in the "nop" bytes above
    :fetch_pointer kernal_stop_vector : #subroutine_runstop_check_replacement
    cli
    rts
    // end of "subroutine_disable_runstop_check"
    
    behind:
  }
  else // code for the routines was already generated/defined
    jsr subroutine_disable_runstop_check
}

.pseudocommand enable_runstop_check
{
  .if (subroutine_enable_runstop_check != 0 ) // code already defined ?
    jsr subroutine_enable_runstop_check
  // else nothing to do because it is enabled by default
}
