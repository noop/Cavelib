// ----------------------------
// Variables and User Variables 
// ---------------------------- 

* = $0002 "zeropage first usable address" virtual 

// zero page variable definitions and library initialization
// ("fast RAM" variables)    
// 
.if ( include_covert_bitops_loader == 1 )
{
  * = $0002 "zeropage covert bitops loadersytem vars" virtual
  // loader needs at least 8..
  .fill 8, 0
  .if ( ADDITIONAL_ZEROPAGE == 1 )
  {
    // ..another 4 in some configs
    .fill 4, 0
  }
}
* = * "zeropage cavelib vars" virtual
cl_w0: .word $FFFF // general purpose 16 bit value 0
cl_w1: .word $FFFF // general purpose 16 bit value 1
cl_w2: .word $FFFF // general purpose 16 bit value 2
cl_w3: .word $FFFF // general purpose 16 bit value 3
cl_b0: .byte $FF   // general purpose  8 bit value 0
cl_b1: .byte $FF   // general purpose  8 bit value 1
cl_b2: .byte $FF   // general purpose  8 bit value 2
cl_b3: .byte $FF   // general purpose  8 bit value 3
cl_b4: .byte $FF   // general purpose  8 bit value 4
// TODO: add more cavelib variables here

.label cavelib_vars_end = *

.var cv_clear_pattern = $00
.var cavelib_user_vars_end = $00

.var cavelib_routines_ready = 0

// set all cavelib vars to the clear pattern
.pseudocommand init_cavelib_vars 
{
  :zp_memset #$02 : #cavelib_vars_end-2 : #cv_clear_pattern
}


// set all cavelib user vars to the clear pattern
.pseudocommand init_cavelib_user_vars
{
  .if (cavelib_user_vars_end == $00 || cavelib_user_vars_begin == $00 )
  {
    .print "zeropage variable definitions example:"
    .print ":begin_zp_user_vars"
    .print "my_array: .fill $20, 0"
    .print ":end_zp_user_vars"
    .error "cavelib user must enclose zero page variables as shown above" 
  }
   
  .if ( cavelib_user_vars_end-1 > $ff )
  {
    .printnow "first zeropage user var: " + address_of(cavelib_user_vars_begin)
    .printnow "   address behind those: " + address_of(cavelib_user_vars_end)
  
    .error "zp user vars may not exceed $" + toHexString(cavelib_vars_end,2) + "-$ff range" 
  }
  
  :zp_memset #cavelib_vars_end : #cavelib_user_vars_end-cavelib_vars_end: #cv_clear_pattern
}


// the following pseudocommand must be used to set the .pc in code  
// so that variables will then be located after the cavelib
// variables inside zero page RAM
// NOTE: by design, none of these vars can be initialized at compile-time
//       because they are located in the zero page locations used by BASIC
//       so all zero page variables have to be initialized manually at
//       runtime after BASIC ROM has been disabled (e.g. by :init_cavelib
.var cavelib_user_vars_begin = $00
.pseudocommand begin_zp_user_vars 
{ 
  * = cavelib_vars_end "zeropage user vars" virtual 
  .eval cavelib_user_vars_begin = *
}


// this pseudocommand must be placed after the user zero page variables
.pseudocommand end_zp_user_vars
{
  .eval cavelib_user_vars_end = *
  
  // generate some memory map info for covert bitops loader system here
  .if ( include_covert_bitops_loader == 1 )
  {
    * = loadbuffer "covert bitops loadersystem loadbuffer" virtual
    .fill $100, 0
    
    .if ( LOADFILE_PUCRUNCH > 0 )
    {
      * = depackbuffer "covert bitops loadersystem pucrunch depackbuffer" virtual
      .fill $1F, 0
    }
    .if ( LOADFILE_EXOMIZER > 0 )
    {
      * = depackbuffer "covert bitops loadersystem exomizer depackbuffer" virtual
      .fill $9C, 0
    }
  }
}


// start defining non zero page variables and routines after using this command 
// and they will be placed shortly behind the Basic Upstart 
//
// pure_virtual : 0 generate code, else do not generate any actual code
.pseudocommand begin_slow_ram_vars_and_routines_x pure_virtual
{
 .if( cavelib_routines_ready == 0)
 {
  .eval cavelib_routines_ready = 1

  .if (pure_virtual.getValue() == 0)
  {
    * = $0810 "slow ram cavelib - vars and routines"
  }
  else
  {
    * = $0810 "slow ram cavelib - vars and routines (virtual)" virtual
  }
  
  .eval cavelib_basic_zp_saved = *   
  .if (pure_virtual.getValue() == 0)
  {
    * = * "slow ram cavelib - basic working area store/restore"
  }
  else
  {
    * = * "slow ram cavelib - basic working area store/restore (virtual)" virtual
  }
   
  .fill $8e , 0 // for storing/restoring BASIC working area 02-8f from zeropage
  
  .eval cavelib_kernal_zp_saved = * 
  .if (pure_virtual.getValue() == 0)
  {
    * = * "slow ram cavelib - kernal working area store/restore"
  }
  else
  {
    * = * "slow ram cavelib - kernal working area store/restore (virtual)" virtual
  }
  .fill $70 , 0 // for storing/restoring KERNAL working area 90-ff from zeropage 
  
  // place mem subroutines
  .if (pure_virtual.getValue() == 0)
  {
    * = * "slow ram cavelib - memory subroutines"
  }
  else
  {
    * = * "slow ram cavelib - memory subroutines (virtual)" virtual
  }
  :place_subroutine_memset
  :place_subroutine_memxor
  :place_subroutine_memcpy
  :place_subroutine_gapcpy
  :place_subroutine_memswap
  
  // place irq subroutines
  .if (pure_virtual.getValue() == 0)
  {
    * = * "slow ram cavelib - nmi/irq/brk/end_irq handlers"
  }
  else
  {
    * = * "slow ram cavelib - nmi/irq/brk/end_irq handlers (virtual)" virtual
  }
  :place_nmi_handler
  :place_irq_brk_handler
  :place_end_irq_handler
  
  // place misc helper subroutines 
  .if (pure_virtual.getValue() == 0)
  {
    * = * "slow ram cavelib - misc helper subroutines"
  }
  else
  {
    * = * "slow ram cavelib - misc helper subroutines (virtual)" virtual
  }
  :place_subroutine_cstring_length
  :place_subroutine_waste_cycles_all

  // place file subroutines
  .if (pure_virtual.getValue() == 0)
  {
    * = * "slow ram cavelib - kernal file subroutines"
  }
  else
  {
    * = * "slow ram cavelib - kernal file subroutines (virtual)" virtual
  }
  :place_subroutine_load_error_store
  :place_subroutine_load_sync
  
  // place gfx subroutines
  .if (pure_virtual.getValue() == 0)
  {
    * = * "slow ram cavelib - gfx subroutines"
  }
  else
  {
    * = * "slow ram cavelib - gfx subroutines (virtual)" virtual
  }  
  :place_subroutine_copy_char_rom
  :place_subroutine_textout1x2
  :place_subroutine_textout1x2_to_sprites

  // TODO: place more cavelib specific variables and routines here
  
  // place 3-rd party subroutines
  .if ( include_covert_bitops_loader == 1 )
  {
    .if (pure_virtual.getValue() == 0)
    {
      * = * "slow ram cavelib - Covert Bitops Loadersystem V2.26ka"
    }
    else
    {
      * = * "slow ram cavelib - Covert Bitops Loadersystem V2.26ka (virtual)" virtual
    }  
    :place_covert_bitops_loader_code
  }
  
  .if (pure_virtual.getValue() == 0)
  {
    * = * "slow ram user - vars and routines"
  }
  
 }
}


// makes sure cavelib subroutines are known to the including code
// but without generating any code (meant for use with runtime loaded code)
.pseudocommand use_virtual_cavelib
{
  :begin_slow_ram_vars_and_routines_x #$01
}


// backward compatible command (includes cavelib code regularly)
.pseudocommand begin_slow_ram_vars_and_routines
{
  :begin_slow_ram_vars_and_routines_x #$00
}


// place this behind non zero page variables and routines definitions and before the 
// main program code to have the memory map show the address range
// used by the program
.pseudocommand end_slow_ram_vars_and_routines
{
  * = * "main program"
}
